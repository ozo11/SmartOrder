package com.oneleaf.dao;

import com.oneleaf.entity.KitchenDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * KitchenDAO的测试类
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class KitchenDAOTest {

    /**
     * 自动装配待测试接口DAO
     */
    @Autowired
    private KitchenDAO kitchenDAO;

    /**
     * 后厨人员全查询测试
     */
    @Test
    public void testListKitchen(){
        List<KitchenDO> kitchens = kitchenDAO.listKitchen();
        for(KitchenDO kitchen : kitchens){
            System.out.println(kitchen);
        }
    }

    /**
     * 后厨人员条件查询测试
     */
    /*@Test
    public void testGetKitchenByCondition(){
        List<KitchenDO> kitchens = kitchenDAO.listKitchen();
        for(KitchenDO kitchen : kitchens){
            System.out.println(kitchen);
        }
    }*/

    /**
     * 测试分页
     */
    @Test
    public void testListKitchenDOByCondition(){
        KitchenDO kitchenDO = new KitchenDO();
        kitchenDO.setStart(0);
        kitchenDO.setLength(3);
        kitchenDO.setKitchenName("%小%");
        List<KitchenDO> kitchens = kitchenDAO.listKitchenByCondition(kitchenDO);
        for(KitchenDO kitchen : kitchens){
            System.out.println(kitchen);
        }
    }

    /**
     * 根据条件查询后厨人员数据
     * @return
     */
    @Test
    public void testGetKitchenByCondition(){
        KitchenDO kitchenDO = new KitchenDO();
        kitchenDO.setKitchenId(301);
        KitchenDO result = kitchenDAO.getKitchenByCondition(kitchenDO);
        System.out.println(result);

    }
}
