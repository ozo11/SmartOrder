package com.oneleaf.dao;

import com.oneleaf.entity.KitchenDO;
import com.oneleaf.entity.OrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * KitchenDAO的测试类
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class OrderDAOTest {

    @Autowired
    private OrderDao orderDao;

    @Test
    public void testupdateState(){
        OrderDO orderDO = new OrderDO();
        orderDao.updateState(2);
        System.out.println();
    }

}
