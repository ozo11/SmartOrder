package com.oneleaf.dao;

import com.oneleaf.entity.AllUserDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
* UserDAO的测试类
*
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class AllUserDAOTest {
    /**
     * 自动装配待测试接口DAO
     */
    @Autowired
    private AllUserDAO allUserDAO;

    /**
     * 条件查询测试
     */
    @Test
    public void testgetAllUserDOByCondition(){
        AllUserDO condition = new AllUserDO();//手动创建的测试条件
//		condition.setUserId(1);
        condition.setUserNumber("123");
		condition.setUserPassword("123");
        AllUserDO result = allUserDAO.getAllUserDOByCondition(condition);
        System.out.println("testgetAllUserDOByCondition:   " + result);
    }

    /**
     * 添加用户测试
     */
    @Test
    public void testsaveAllUser(){
        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserName("小赵");
        allUserDO.setUserNumber("777");
        allUserDO.setUserPassword("123");
        allUserDO.setUserImageUrl("D:/1JAVAOnline/SmartOrder");
        allUserDO.setUserType("kitchen");
        System.out.println("testsaveallUserDO:   " + allUserDO);
        allUserDAO.saveAllUser(allUserDO);
        AllUserDO result = allUserDAO.getAllUserDOByCondition(allUserDO);
        System.out.println("testsaveallUserDO:   " + result);
    }

    /**
     * 修改用户测试
     */
    @Test
    public void testupdateAllUser(){
        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserId(306);
        allUserDO.setUserName("后厨7");
        allUserDO.setUserNumber("k307");
        allUserDO.setUserPassword("123");
        allUserDO.setUserImageUrl("D:/1JAVAOnline/SmartOrder");
        allUserDO.setUserType("kitchen");
        System.out.println("testsaveallUserDO:   " + allUserDO);
        allUserDAO.updateAllUser(allUserDO);
        AllUserDO result = allUserDAO.getAllUserDOByCondition(allUserDO);
        System.out.println("testsaveallUserDO:   " + result);
    }

}
