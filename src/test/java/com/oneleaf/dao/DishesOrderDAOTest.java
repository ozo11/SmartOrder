package com.oneleaf.dao;

import com.oneleaf.entity.DishesOrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * DishesOrderDAO的测试类
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class DishesOrderDAOTest {

    /**
     * 自动装配待测试接口DAO
     */
    @Autowired
    private DishesOrderDAO dishesOrderDAO;

    @Test
    public void testlistdishesorder(){
        List<DishesOrderDO> orders=dishesOrderDAO.listDishesorder();
        for(DishesOrderDO order: orders){
            System.out.println(order);
        }
    }
    @Test
    public void testlistCookedOrder(){
        List<DishesOrderDO> orders1=dishesOrderDAO.listCookedOrder();
        for(DishesOrderDO order1: orders1){
            System.out.println(order1);
        }
    }
    @Test
    public void teststartDishesOrder(){
        DishesOrderDO dishesOrder = new DishesOrderDO();
        dishesOrder.setDishesOrderId(1);
        dishesOrderDAO.startDishesOrder(dishesOrder);
    }
    @Test
    public void testendDishesOrder(){
        DishesOrderDO dishesOrder = new DishesOrderDO();
        dishesOrder.setDishesOrderId(1);
        dishesOrderDAO.endDishesOrder(dishesOrder);
    }
}
