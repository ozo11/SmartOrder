package com.oneleaf.service;

import com.oneleaf.entity.DishesOrderDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class DishesOrderServiceTest {
    @Autowired
    private DishesOrderService dishesOrderService;

    @Test
    public void testlistdishesorder(){
        List<DishesOrderDO> orders=dishesOrderService.listDishesorder();
        for(DishesOrderDO order: orders){
            System.out.println(order);
        }
    }
    @Test
    public void testlistCookedOrder(){
        List<DishesOrderDO> orders1=dishesOrderService.listCookedOrder();
        for(DishesOrderDO order1: orders1){
            System.out.println(order1);
        }
    }
    @Test
    public void teststartDishesOrder(){
        DishesOrderDO dishesOrder = new DishesOrderDO();
        dishesOrder.setDishesOrderId(1);
        dishesOrderService.startDishesOrder(dishesOrder);
    }
    @Test
    public void testendDishesOrder(){
        DishesOrderDO dishesOrder = new DishesOrderDO();
        dishesOrder.setDishesOrderId(1);
        dishesOrderService.endDishesOrder(dishesOrder);
    }
}
