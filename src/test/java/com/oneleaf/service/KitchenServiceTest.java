package com.oneleaf.service;

import com.oneleaf.dao.KitchenDAO;
import com.oneleaf.entity.KitchenDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class KitchenServiceTest {

    @Autowired
    private KitchenService kitchenService;

    @Test
    public void testListKitchen() {
        List<KitchenDO> kitchens = kitchenService.listKitchen();
        for (KitchenDO kitchen : kitchens) {
            System.out.println(kitchen);
        }
    }

    /**
     * 测试分页
     */
    @Test
    public void testListKitchenDOByCondition(){
        KitchenDO kitchenDO = new KitchenDO();
        kitchenDO.setStart(0);
        kitchenDO.setLength(3);
        kitchenDO.setKitchenName("%小%");
        List<KitchenDO> kitchens = kitchenService.listKitchenByCondition(kitchenDO);
        for(KitchenDO kitchen : kitchens){
            System.out.println(kitchen);
        }
    }


    /**
     * 根据条件查询后厨人员数据
     * @return
     */
    @Test
    public void testGetKitchenByCondition(){
        KitchenDO kitchenDO = new KitchenDO();
        kitchenDO.setKitchenId(301);
        KitchenDO result = kitchenService.getUpdateKitchen(kitchenDO);
        System.out.println(result);

    }
}
