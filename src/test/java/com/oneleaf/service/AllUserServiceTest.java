package com.oneleaf.service;

import com.oneleaf.entity.AllUserDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 用户业务层接口的单元测试类
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:config/application-*.xml")
public class AllUserServiceTest {
    @Autowired
    private AllUserService allUserService;

    @Test
    public void testLogin(){
        AllUserDO allUserDO = new AllUserDO();
//		userDO.setUserId(1);
        allUserDO.setUserNumber("123");
		allUserDO.setUserPassword("123");
        allUserDO.setUserType("admin");
        System.out.println("AllUserDOServiceTest   " + allUserService.login(allUserDO));
    }

    @Test
    public void testsaveAllUser(){

        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserName("小王");
        allUserDO.setUserNumber("777");
        allUserDO.setUserPassword("123");
        allUserDO.setUserImageUrl("D:/1JAVAOnline/SmartOrder");
        allUserDO.setUserType("kitchen");
        System.out.println("testsaveallUserDO:   " + allUserDO);
        allUserService.saveAllUser(allUserDO);
        AllUserDO result = allUserService.login(allUserDO);
        System.out.println("testsaveallUserDO:   " + result);
    }
}
