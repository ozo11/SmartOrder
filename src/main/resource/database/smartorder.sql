/*
 Navicat Premium Data Transfer

 Source Server         : ozo1
 Source Server Type    : MySQL
 Source Server Version : 80016
 Source Host           : localhost:3306
 Source Schema         : smartorder

 Target Server Type    : MySQL
 Target Server Version : 80016
 File Encoding         : 65001

 Date: 23/05/2022 22:36:08
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin`  (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `admin_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `admin_phonenum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES (1, 1, '管理1', '180');
INSERT INTO `admin` VALUES (2, 2, '管理2', '170');

-- ----------------------------
-- Table structure for alluser
-- ----------------------------
DROP TABLE IF EXISTS `alluser`;
CREATE TABLE `alluser`  (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_number` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 338 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alluser
-- ----------------------------
INSERT INTO `alluser` VALUES (101, '管理1', 'a123', '123', 'a1', 'admin');
INSERT INTO `alluser` VALUES (201, '服务1', 'w201', '123', 'w1', 'waiter');
INSERT INTO `alluser` VALUES (202, '服务2', 'w202', '123', 'w1', 'waiter');
INSERT INTO `alluser` VALUES (203, '服务3', 'w203', '12356', 'w3', 'waiter');
INSERT INTO `alluser` VALUES (204, '服务4', 'w204', '123', 'w4', 'waiter');
INSERT INTO `alluser` VALUES (205, '服务5', 'w205', '123', 'w5', 'waiter');
INSERT INTO `alluser` VALUES (301, '后厨1', 'k301', '123', 'k1', 'kitchen');
INSERT INTO `alluser` VALUES (302, '后厨2', 'k302', '123', 'w7', 'kitchen');
INSERT INTO `alluser` VALUES (303, '后厨3', 'k303', '1234', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\userImage\\1596125151495.jpg', 'kitchen');
INSERT INTO `alluser` VALUES (307, '后厨7', 'k307', '123', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\userImage\\1596125151495.jpg', 'kitchen');
INSERT INTO `alluser` VALUES (308, '后厨8', 'k308', '123', '12', 'kithcen');
INSERT INTO `alluser` VALUES (337, '小康', 'w444', '123', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\userImage\\1596225010904.jpg', 'waiter');
INSERT INTO `alluser` VALUES (338, 'hh', '999', '123', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\userImage\\1596248800735.jpg', 'waiter');

-- ----------------------------
-- Table structure for dishes
-- ----------------------------
DROP TABLE IF EXISTS `dishes`;
CREATE TABLE `dishes`  (
  `dishes_id` int(11) NOT NULL AUTO_INCREMENT,
  `dishes_type_id` int(11) NULL DEFAULT NULL,
  `dishes_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dishes_introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dishes_detail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dishes_recommend` int(11) NULL DEFAULT NULL,
  `dishes_price` decimal(10, 2) NULL DEFAULT NULL,
  `dishes_inventory_status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`dishes_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dishes
-- ----------------------------
INSERT INTO `dishes` VALUES (1, 1, '酸菜鱼', '又酸又菜有多余', '酸菜加鱼', 3, 200.00, 200);
INSERT INTO `dishes` VALUES (2, 1, '鱼香肉丝', '鱼跟肉丝', '不好吃，别买', 1, 20.00, NULL);
INSERT INTO `dishes` VALUES (3, 1, '黄焖鸡', '又黄有焖又骚鸡', '勉强可以吃', 2, 20.00, NULL);
INSERT INTO `dishes` VALUES (4, 2, '小炒肉', '又小有吵又多肉', '肉少，别买e', 2, 60.00, NULL);
INSERT INTO `dishes` VALUES (5, 3, '香辣鱼', '不香不辣还多余', '孜然味的香辣', 3, 30.00, NULL);

-- ----------------------------
-- Table structure for dishes_image
-- ----------------------------
DROP TABLE IF EXISTS `dishes_image`;
CREATE TABLE `dishes_image`  (
  `dishes_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `dishes_id` int(11) NULL DEFAULT NULL,
  `dishes_image_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dishes_image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dishes_image_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dishes_image
-- ----------------------------
INSERT INTO `dishes_image` VALUES (22, 1, '酸菜鱼.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\酸菜鱼.jpg');
INSERT INTO `dishes_image` VALUES (23, 2, '鱼香肉丝.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\鱼香肉丝.jpg');
INSERT INTO `dishes_image` VALUES (24, 3, '黄焖鸡.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\黄焖鸡.jpg');
INSERT INTO `dishes_image` VALUES (25, 4, '小炒肉.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\小炒肉.jpg');
INSERT INTO `dishes_image` VALUES (26, 5, '香辣鱼.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\香辣鱼.jpg');
INSERT INTO `dishes_image` VALUES (35, 4, '鱼香肉丝1.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\鱼香肉丝1.jpg');
INSERT INTO `dishes_image` VALUES (36, 5, '香辣鱼1.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\香辣鱼1.jpg');
INSERT INTO `dishes_image` VALUES (37, 20, '鱼香肉丝.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\鱼香肉丝.jpg');
INSERT INTO `dishes_image` VALUES (38, 21, '香辣鱼1.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\香辣鱼1.jpg');
INSERT INTO `dishes_image` VALUES (39, 21, '小炒肉.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\小炒肉.jpg');
INSERT INTO `dishes_image` VALUES (40, 22, '土豆丝.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\土豆丝.jpg');
INSERT INTO `dishes_image` VALUES (41, 22, '土豆丝1.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\土豆丝1.jpg');
INSERT INTO `dishes_image` VALUES (42, 22, '土豆丝2.jpg', 'D:\\1JavaOnline\\SmartOrder\\target\\SmartOrder\\uploadMultipal\\土豆丝2.jpg');

-- ----------------------------
-- Table structure for dishes_order
-- ----------------------------
DROP TABLE IF EXISTS `dishes_order`;
CREATE TABLE `dishes_order`  (
  `dishes_order_id` int(11) NOT NULL AUTO_INCREMENT,
  `dishes_id` int(11) NULL DEFAULT NULL,
  `dishes_amount` int(11) NULL DEFAULT NULL,
  `dishes_start_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waiter_id` int(11) NULL DEFAULT NULL,
  `dishes_status` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `table_num` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`dishes_order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dishes_order
-- ----------------------------
INSERT INTO `dishes_order` VALUES (60, 4, 2, '2020-07-30 21:19:55', 201, '烹饪完成', 6);
INSERT INTO `dishes_order` VALUES (61, 5, 1, '2020-07-30 21:19:55', 201, '烹饪完成', 6);
INSERT INTO `dishes_order` VALUES (64, 3, 2, '2020-07-30 22:20:05', 202, '烹饪完成', 9);
INSERT INTO `dishes_order` VALUES (97, 5, 1, '2020-08-01 00:47:26', 201, '烹饪完成', 8);
INSERT INTO `dishes_order` VALUES (98, 2, 1, '2020-08-01 01:56:54', 201, '烹饪完成', 5);
INSERT INTO `dishes_order` VALUES (106, 3, 1, '2020-08-01 10:29:47', 201, '烹饪完成', 17);
INSERT INTO `dishes_order` VALUES (107, 4, 1, '2020-08-01 10:29:47', 201, '烹饪完成', 17);
INSERT INTO `dishes_order` VALUES (108, 5, 1, '2020-08-01 10:29:47', 201, '烹饪完成', 17);

-- ----------------------------
-- Table structure for dishes_type
-- ----------------------------
DROP TABLE IF EXISTS `dishes_type`;
CREATE TABLE `dishes_type`  (
  `dishes_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `dishes_type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dishes_type_taste` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dishes_type_introduction` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`dishes_type_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dishes_type
-- ----------------------------
INSERT INTO `dishes_type` VALUES (1, '川菜', '辣', '安达市多');
INSERT INTO `dishes_type` VALUES (2, '粤菜', '甜', '啊打广告');
INSERT INTO `dishes_type` VALUES (3, '小面', '辣', '阿达');
INSERT INTO `dishes_type` VALUES (4, '家常菜', '好', '很好吃');

-- ----------------------------
-- Table structure for kitchen
-- ----------------------------
DROP TABLE IF EXISTS `kitchen`;
CREATE TABLE `kitchen`  (
  `kitchen_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `kitchen_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kitchen_sex` int(11) NULL DEFAULT NULL,
  `kitchen_age` int(11) NULL DEFAULT NULL,
  `kitchen_entry_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `kitchen_phonenum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kitchen_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 308 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of kitchen
-- ----------------------------
INSERT INTO `kitchen` VALUES (301, 301, '小王', 2, 29, '2020-07-11', '13333335533');
INSERT INTO `kitchen` VALUES (302, 302, '小李', 2, 19, '2020-07-29', '18899996666');
INSERT INTO `kitchen` VALUES (303, 303, '王路', 1, 20, '2020-07-30', '112555555');
INSERT INTO `kitchen` VALUES (307, 307, '小娜', 2, 27, '2020-07-10', '1447722994');
INSERT INTO `kitchen` VALUES (308, 308, '王拉', 1, 22, '2020-07-10', '1112223334');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `notice_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NULL DEFAULT NULL,
  `notice_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notice_content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 42 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES (1, 101, '2020-07-19', '请注意添加菜品');
INSERT INTO `notice` VALUES (2, 101, '2020-07-20', '请注意添加员工');
INSERT INTO `notice` VALUES (3, 101, '2020-07-29', '请注意订单');
INSERT INTO `notice` VALUES (4, 101, '2020-07-31', '请注意本店周年庆');
INSERT INTO `notice` VALUES (5, 101, '2020-07-31', '请注意');
INSERT INTO `notice` VALUES (6, 101, '2020-7-31', '大酬宾');

-- ----------------------------
-- Table structure for order_dishes
-- ----------------------------
DROP TABLE IF EXISTS `order_dishes`;
CREATE TABLE `order_dishes`  (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_sum` decimal(10, 2) NULL DEFAULT NULL,
  `order_create_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_end_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `order_detile` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `table_num` int(11) NULL DEFAULT NULL,
  `order_status` int(11) NULL DEFAULT NULL,
  `waiter_id` int(11) NULL DEFAULT NULL,
  `dishes_order_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of order_dishes
-- ----------------------------
INSERT INTO `order_dishes` VALUES (7, 430.00, '2020-07-30 21:19:55', '2020-07-31 20:48:40', '酸菜鱼:1\r\n鱼香肉丝:3酸菜鱼:1\n\n', 6, 2, 201, NULL);
INSERT INTO `order_dishes` VALUES (8, 610.00, '2020-07-30 22:20:05', '2020-07-31 20:48:44', '酸菜鱼:2\n鱼香肉丝:3\n黄焖鸡:2\n小炒肉:2\n香辣鱼:1\n', 3, 2, 201, NULL);
INSERT INTO `order_dishes` VALUES (9, 220.00, '2020-07-30 23:06:49', NULL, '酸菜鱼:2\n鱼香肉丝:3\n黄焖鸡:2\n小炒肉:2\n香辣鱼:1\n酸菜鱼:1\n鱼香肉丝:1\n', 3, 3, 201, NULL);
INSERT INTO `order_dishes` VALUES (10, 200.00, '2020-07-30 23:08:30', '2020-08-01 01:03:36', '酸菜鱼:2\n鱼香肉丝:3\n黄焖鸡:2\n小炒肉:2\n\n酸菜鱼:1\n鱼香肉丝:1\n酸菜鱼:1\n', 9, 3, 201, NULL);
INSERT INTO `order_dishes` VALUES (11, 380.00, '2020-07-30 23:09:43', '2020-08-01 00:39:39', '酸菜鱼:1\n鱼香肉丝:1\n黄焖鸡:2\n小炒肉:3\n', 12, 2, 201, NULL);
INSERT INTO `order_dishes` VALUES (12, 310.00, '2020-07-30 23:11:21', NULL, '酸菜鱼:1\n鱼香肉丝:1\n黄焖鸡:1\n小炒肉:1\n香辣鱼:1\n', 13, 1, 201, NULL);
INSERT INTO `order_dishes` VALUES (21, 30.00, '2020-07-31 22:56:36', '2020-08-01 02:28:12', '黄焖鸡:1\n香辣鱼:1\n', 4, 3, 201, NULL);
INSERT INTO `order_dishes` VALUES (22, 90.00, '2020-08-01 00:47:26', '2020-08-01 02:01:39', '小炒肉:1\n香辣鱼:1\n', 8, 3, 201, NULL);
INSERT INTO `order_dishes` VALUES (23, 160.00, '2020-08-01 01:56:54', '2020-08-01 02:01:37', '鱼香肉丝:1\n黄焖鸡:1\n小炒肉:2\n', 5, 3, 201, NULL);
INSERT INTO `order_dishes` VALUES (24, 240.00, '2020-08-01 02:26:48', NULL, '香辣鱼:1\n酸菜鱼:1\n鱼香肉丝:1\n黄焖鸡:1\n', 8, 1, 201, NULL);
INSERT INTO `order_dishes` VALUES (25, 90.00, '2020-08-01 02:27:01', NULL, '黄焖鸡:2\n小炒肉:1\n香辣鱼:1\n', 9, 1, 201, NULL);
INSERT INTO `order_dishes` VALUES (26, 110.00, '2020-08-01 10:29:47', '2020-08-01 10:30:36', '黄焖鸡:1\n小炒肉:1\n香辣鱼:1\n', 17, 3, 201, NULL);

-- ----------------------------
-- Table structure for user_image
-- ----------------------------
DROP TABLE IF EXISTS `user_image`;
CREATE TABLE `user_image`  (
  `user_image_id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_image_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for waiter
-- ----------------------------
DROP TABLE IF EXISTS `waiter`;
CREATE TABLE `waiter`  (
  `waiter_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `waiter_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waiter_sex` int(11) NULL DEFAULT NULL,
  `waiter_age` int(11) NULL DEFAULT NULL,
  `waiter_entry_time` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `waiter_phonenum` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`waiter_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 209 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of waiter
-- ----------------------------
INSERT INTO `waiter` VALUES (201, 201, '大王', 1, 22, '2020-06-10', '19974445555');
INSERT INTO `waiter` VALUES (202, 202, '大新', 1, 21, '2020-06-27', '19977775588');
INSERT INTO `waiter` VALUES (203, 203, '大欧', 1, 27, '2020-06-28', '12277775522');
INSERT INTO `waiter` VALUES (204, 204, '大普', 2, 22, '2020-06-18', '18833335555');
INSERT INTO `waiter` VALUES (205, 205, '大卡', 1, 22, '2020-06-11', '19944475533');

SET FOREIGN_KEY_CHECKS = 1;
