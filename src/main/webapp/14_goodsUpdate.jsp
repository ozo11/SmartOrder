<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<!--国产浏览器高速模式-->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- 定义页面的最新版本 -->
		<meta name="description" content="网站简介" />
		<!-- 网站简介 -->
		<meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
		<title>菜单信息更新</title>

		<!-- 公共样式 开始 -->
		<link rel="stylesheet" type="text/css" href="${basePath}css/base.css">
		<link rel="stylesheet" type="text/css" href="${basePath}css/iconfont.css">
		<script type="text/javascript" src="${basePath}framework/jquery-1.11.3.min.js"></script>
		<link rel="stylesheet" type="text/css" href="${basePath}layui/css/layui.css">
		<script type="text/javascript" src="${basePath}layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="${basePath}css/jquery.mCustomScrollbar.css">
		<script src="${basePath}framework/jquery-ui-1.10.4.min.js"></script>
		<script src="${basePath}framework/jquery.mousewheel.min.js"></script>
		<script src="${basePath}framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="${basePath}framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->
		
		<style>
			.layui-form-label{
				width: 100px;
			}
			.layui-input-block{
				margin-left: 130px;
			}
		</style>

	</head>

	<body>
	<div id="panel_login" style="width: 500px;">
			<form id="addForm" class="layui-form" action="${basePath}food/updateFood" method="post">
				<div class="layui-form-item">
					<label class="layui-form-label">菜品编号</label>
					<div class="layui-input-block">
						<input type="text" name="dishesId" required lay-verify="required" autocomplete="off" class="layui-input" value="${dishes.dishesId}" readonly>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">菜品名称</label>
					<div class="layui-input-block">
						<input type="text" name="dishesName" required lay-verify="required" autocomplete="off" class="layui-input" value="${dishes.dishesName}">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">菜品简介</label>
					<div class="layui-input-block">
						<input type="text" name="dishesIntroduction" required lay-verify="required" autocomplete="off" class="layui-input" value="${dishes.dishesIntroduction}">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">菜品价格</label>
					<div class="layui-input-block">
						<input type="text" name="dishesPrice" autocomplete="off" class="layui-input" value="${dishes.dishesPrice}">单位：元
					</div>
				</div>
				
				<div class="layui-form-item">
					<label class="layui-form-label" >简单描述</label>
					<div class="layui-input-block">
						<input type="text" name="dishesDetail" required lay-verify="required" autocomplete="off" class="layui-input" value="${dishes.dishesDetail}">
					</div>
				</div>
				<br>
				<div class="layui-row">
					<div class="layui-col-md3">
					<label class="input-group-addon" style="margin-left: 60px">推荐等级</label>
					</div>
					<div class="layui-col-md5" style="margin-left: 5px">
					<select name="dishesRecommend">
						<option value="1"  <c:if test="${dishesRecommend=='1'}" ></c:if>>一星</option>
						<option value="2" <c:if test="${dishesRecommend=='2'}"></c:if>>二星</option>
						<option value="3" <c:if test="${dishesRecommend=='3'}"></c:if>>三星</option>
						<option value="4" <c:if test="${dishesRecommend=='4'}"></c:if>>四星</option>
						<option value="5" <c:if test="${dishesRecommend=='5'}"></c:if>>五星</option>
					</select>
					</div>
				</div>
				
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn" lay-submit lay-filter="submitBut" style="margin-top: 40px">立即提交</button>
					</div>
				</div>
			</form>
			
			
			<script>
				layui.use(['upload','form'], function() {
					var form = layui.form;
					var upload = layui.upload;
					var layer = layui.layer;
					//监听提交
					//解决了layui.open弹窗从内部关闭这个弹窗的问题
					form.on('submit(submitBut)', function(data) {
						var updateFrame = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(updateFrame);  //再改变当前层的标题
					});
					form.verify({
						//数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
					  	ZHCheck: [
						    /^[\u0391-\uFFE5]+$/
						    ,'只允许输入中文'
					  	] 
					});
					//拖拽上传
					upload.render({
						elem: '#goodsPic',
						url: '/upload/',
						done: function(res) {
						  	console.log(res)
						}
					});
				});
			</script>

		</div>
	</body>

</html>