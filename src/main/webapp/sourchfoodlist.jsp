<%--
  Created by IntelliJ IDEA.
  User: 胡中宝
  Date: 2020/7/29
  Time: 1:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>

<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <title>菜单列表</title>

    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="images/login_bg2.ico"/>
    <link rel="bookmark" href="images/login_bg2.ico"/>
    <link rel="stylesheet" type="text/css" href="${basePath}css/base.css">
    <link rel="stylesheet" type="text/css" href="${basePath}css/iconfont.css">
    <script type="text/javascript" src="${basePath}framework/jquery-1.11.3.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="${basePath}layui/css/layui.css">
    <!--[if lt IE 9]>
    <script src="${basePath}framework/html5shiv.min.js"></script>
    <script src="${basePath}framework/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="${basePath}layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="${basePath}css/jquery.mCustomScrollbar.css">
    <script src="${basePath}framework/jquery-ui-1.10.4.min.js"></script>
    <script src="${basePath}framework/jquery.mousewheel.min.js"></script>
    <script src="${basePath}framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="${basePath}framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <link rel="stylesheet" type="text/css" href="${basePath}css/frameStyle.css">
    <script type="text/javascript" src="${basePath}framework/frame.js" ></script>

    <style>
        .layui-table img {
            max-width: none;
        }
    </style>

</head>

<body>
<div class="cBody">
    <div class="console">
        <form class="layui-form" action="${basePath}food/selectFood" method="post">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" name="sourch"  placeholder="输入菜品名称" autocomplete="off" class="layui-input">
                </div>
                <%--<input type="submit" value="检索"/>--%>
                <button class="layui-btn" lay-submit lay-filter="searchkitchen">查询</button>
            </div>
        </form>

        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    return false;
                });
            });
        </script>
    </div>

    <table class="layui-table" id="customList">
        <tr>
            <th>菜品编号</th>
            <th>菜品图片</th>
            <th>菜品名称</th>
            <th>菜品简介</th>
            <th>菜品价格(元)</th>
            <th>推荐指数(颗星)</th>
            <th>操作</th>
        </tr>

        <c:forEach items="${list}" var="dishesDo">
            <tr>
                <td>${dishesDo.dishesId}</td>
                <td><a href="${basePath }food/findAllPicture?id=${dishesDo.dishesId}" >查看菜品图片</a></td>
                <td>${dishesDo.dishesName}</td>
                <td>${dishesDo.dishesIntroduction}</td>
                <td>${dishesDo.dishesPrice}</td>
                <td>${dishesDo.dishesRecommend}</td>
                <td>
                    <a href="${basePath }food/findFile?id=${dishesDo.dishesId}">上传图片</a>
                    <a href="${basePath }food/findFoodById?id=${dishesDo.dishesId}">修改</a>
                    <a href="javascript:void(0)" onclick="confirmDel()">删除</a>
                    <script type="text/javascript">
                        function confirmDel()
                        {
                            if(window.confirm("确定删除这条记录吗？")){
                                document.location="${basePath }food/deleteFood?id=${dishesDo.dishesId}"
                            }
                        }
                    </script>
                </td>
            <tr/>
        </c:forEach>

    </table>
    <div id="pages"></div>
    <script>
        layui.use('laypage', function() {
            var laypage = layui.laypage;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: 20
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
//					      console.log(obj)
                }
            });
        });

            layui.use('layer', function() {
                var layer = layui.layer;
                //iframe层-父子操作
                layer.open({
                    type: 2,
                    area: ['60%', '50%'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: 'addfood.jsp'
                });
            });

            //重新初始化Iframe的高度
            cframeInit();
        }
        //修改规格
        function specificationsBut(){
            layui.use('layer', function() {
                var layer = layui.layer;

                //iframe层-父子操作
                layer.open({
                    type: 2,
                    area: ['70%', '60%'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: '10_loading.jsp'
                });
            });

        }

        //删除
        function delCustomList(_this){
            layui.use(['form','laydate'], function() {
                layer.confirm('确定要删除么？', {
                    btn: ['确定', '取消'] //按钮
                }, function() {
                    $(_this).parent().parent().remove();
                    layer.msg('删除成功', {
                        icon: 1
                    });
                }, function() {
                    layer.msg('取消删除', {
                        time: 2000 //20s后自动关闭
                    });
                });
            });
        }
        //修改按钮
        var updateFrame = null;
        function updateBut(){
            layui.use('layer', function() {
                var layer = layui.layer;

                //iframe层-父子操作
                updateFrame = layer.open({
                    title: "商品信息修改",
                    type: 2,
                    area: ['70%', '60%'],
                    scrollbar: false,	//默认：true,默认允许浏览器滚动，如果设定scrollbar: false，则屏蔽
                    maxmin: true,
                    content: '${basePath}food/'
                });
            });

        }
    </script>
</div>
</body>

</html>