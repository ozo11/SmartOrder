<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<title>员工信息更新</title>

		<!-- 公共样式 开始 -->
		<link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
		<link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
		<script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js"></script>
		<link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
		<script type="text/javascript" src="${basePath }layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
		<script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
		<script src="${basePath }framework/jquery.mousewheel.min.js"></script>
		<script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->
		
		<style>
			.layui-form-label{
				width: 100px;
			}
			.layui-input-block{
				margin-left: 130px;
			}
		</style>

	</head>

	<body>
    <div id="panel_login" style="width: 500px;">
			<form id="addForm" class="layui-form" action="${basePath }waiter/doupdate.do" >
				<input type="hidden" name="allUserDO.userId" value="${allUser.userId }"/>
				<input type="hidden" name="waiterId" value="${updateUser.waiterId }"/>
				<%--<div class="layui-form-item">
					<label class="layui-form-label">员工工号</label>
					<div class="layui-input-block">
						<input type="number" name="waiterId" value="${updateUser.waiterId }" required lay-verify="required" autocomplete="off" class="layui-input">
					</div>
				</div>--%>
				<div class="layui-form-item">
					<label class="layui-form-label">用户姓名</label>
					<div class="layui-input-block">
						<input type="text" name="allUserDO.userName" value="${allUser.userName }" required lay-verify="required" autocomplete="off" class="layui-input" onchange="updateUser()">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">用户账号</label>
					<div class="layui-input-block">
						<input type="text" name="allUserDO.userNumber" value="${allUser.userNumber }" required lay-verify="required" autocomplete="off" class="layui-input" onchange="updateUser()">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">用户密码</label>
					<div class="layui-input-block">
						<input type="text" name="allUserDO.userPassword" value="${allUser.userPassword }" required lay-verify="required" autocomplete="off" class="layui-input" onchange="updateUser()">
					</div>
				</div>
				<div class="layui-form-item" lay-filter="typeselect">
					<label class="layui-form-label">员工类型</label>
					<div class="layui-input-block">
					<select class="layui-input-block" name="allUserDO.userType" disabled="disabled">
						<option value="waiter" <c:if test="${allUser.userType == 'waiter'}" >
							selected=="selected"}</c:if>>服务人员</option>
						<option value="waiter" <c:if test="${allUser.userType == 'kitchen'}" >
							selected=="selected"}</c:if>>后厨人员</option>
					</select>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">员工姓名</label>
					<div class="layui-input-block">
						<input type="text" name="waiterName" value="${updateUser.waiterName }" required lay-verify="required" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">员工性别</label>
					<div class="layui-input-block">
						<label class="radio-inline">
							<input type="radio" name="waiterSex" id="inlineRadio1" value="1"
							${updateUser.waiterSex == '1'?'checked':'' }>男
						</label>
						<label class="radio-inline">
							<input type="radio" name="waiterSex" id="inlineRadio2" value="2"
							${updateUser.waiterSex == '2'?'checked':'' }>女
						</label>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">员工年龄</label>
					<div class="layui-input-block">
						<input type="number" name="waiterAge" value="${updateUser.waiterAge }" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">电话号码</label>
					<div class="layui-input-block">
						<input type="text" name="waiterPhonenum" value="${updateUser.waiterPhonenum }" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">入职时间</label>
					<div class="layui-input-block">
						<input type="date" name="waiterEntryTime" value="${updateUser.waiterEntryTime }" autocomplete="off" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<div class="layui-input-block">
						<button class="layui-btn layui-btn-lg" lay-submit lay-filter="updateBut">修改</button>
					</div>
				</div>
			</form>
    </div>>
			
			<script>
				layui.use(['upload','form'], function() {
					var form = layui.form;
					var upload = layui.upload;
					var layer = layui.layer;
					//监听提交
					//解决了layui.open弹窗从内部关闭这个弹窗的问题
					form.on('submit(submitBut)', function(data) {
						var updateFrame = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(updateFrame);  //再改变当前层的标题
					});
					form.verify({
						//数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
					  	ZHCheck: [
						    /^[\u0391-\uFFE5]+$/
						    ,'只允许输入中文'
					  	] 
					});
				});

				//监听提交
				form.on('submit(updateBut)', function(data) {
					//console.log(data);

                    layui.use('jquery',function(){
                        var $=layui.$;
                        $.ajax({
                            type: 'post',
                            url: '${basePath }user/doupdate.do', // ajax请求路径
                            data: {
                                name:data.field.name
                            },
                            success: function(data){
                                if(data=='ok'){
                                    layer.msg('添加成功');
                                }else if(data=='error'){
                                    layer.msg('添加失败');
                                }
                            }
                        });
                    });
                    return false;//禁止跳转，否则会提交两次，且页面会刷新
				});

			</script>


	</body>

</html>