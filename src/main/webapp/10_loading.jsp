<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<title>维护中</title>

		<!-- 公共样式 开始 -->
		<link rel="stylesheet" type="text/css" href="css/base.css">
		<link rel="stylesheet" type="text/css" href="css/iconfont.css">
		<script type="text/javascript" src="framework/jquery-1.11.3.min.js"></script>
		<link rel="stylesheet" type="text/css" href="layui/css/layui.css">
		<script type="text/javascript" src="layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">
		<script src="framework/jquery-ui-1.10.4.min.js"></script>
		<script src="framework/jquery.mousewheel.min.js"></script>
		<script src="framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->
		
		<style>
			body{
				width: 100%;
				height: 100%;
				overflow: hidden;
				background: url(images/other/maintain_bottom.png) no-repeat center bottom;
				background-size: 100% auto;
			}
			.midd{
				width: 500px;
				height: 320px;
				position: absolute;
				top: 50%;
				left: 50%;
				margin-top: -250px;
				margin-left: -250px;
			}
			.midd img{
				width: 500px;
				height: 243px;
			}
			.midd p{
				width: 100%;
				height: 77px;
				line-height: 77px;
				text-align: center;
				font-size: 24px;
				color: #00bfb4;
				background: url(images/other/maintain_pic_bg.png) no-repeat center bottom;
				background-size: 80% auto;
			}
		</style>
	</head>

	<body>
		<div class="midd">
			<img src="images/other/maintain_pic.png" />
			<p>您访问的页面正在升级维护中...</p>
		</div>
		<script>
			$(function(){
				var win_h = $(window).height();
				$("body").height(win_h);
			})
//			$(function(){
//				var maintain_h = $('#mainIframe',parent.document).parents(window).height();
//				console.log(maintain_h)
//				$("body").height(maintain_h);
//			})
		</script>
	</body>

</html>