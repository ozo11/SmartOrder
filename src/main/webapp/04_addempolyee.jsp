<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>
<head>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<!--国产浏览器高速模式-->
		<meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
		<title>登录</title>

		<!-- 公共样式 开始 -->
		<link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
		<link rel="bookmark" href="${basePath }images/ozo.jpg"/>
		<link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
		<link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
		<script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
		<link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
		<!--[if lt IE 9]>
		<script src="${basePath }framework/html5shiv.min.js"></script>
		<script src="${basePath }framework/respond.min.js"></script>
		<![endif]-->
		<script type="text/javascript" src="${basePath }layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
		<script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
		<script src="${basePath }framework/jquery.mousewheel.min.js"></script>
		<script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->

	</head>
</head>
<body style="background-image:url(${basePath }images/222.jpg) ;background-size: cover;">
<div id="panel_login" style="width: 500px;">
<div class="layui-form1" style="padding: 100px 200px 10px;">

	<form class="layui-form" role="form" action="${basePath }user/doadduser.do" method="post">
		<%--<input class="hidden" name="desc" value="${descPath}">--%>
		<br>
		<div class="input-group">
			<span class="input-group-addon" style="color: white;">用户名称</span>
			<input type="text" style="width:300px;height:40px" name="userName" required lay-verify="userName" class="form-control" autocomplete="off" placeholder="请输入用户名称">
		</div>
		<br>
		<div class="input-group">
			<span class="input-group-addon" style="color: white;">用户账号</span>
			<input type="text" style="width:300px;height:40px" name="userNumber" required lay-verify="userNumber" autocomplete="off" class="form-control" placeholder="请输入用户账号">
		</div>
		<br>
		<div class="input-group">
			<span class="input-group-addon" style="color: white;">用户密码</span>
			<input type="password" style="width:300px;height:40px" name="userPassword" lay-verify="required" autocomplete="off" class="form-control" placeholder="请输入用户密码">
		</div>
        <br>
		<div class="input-group">
			<span class="input-group-addon" style="color: white;">确认密码</span>
			<input type="password" style="width:300px;height:40px" lay-verify="required|confirmPass" autocomplete="off" class="form-control" placeholder="请输入用户密码">
		</div>
		<br>
		<div class="input-group" lay-filter="typeselect1">
            <span class="input-group-addon" style="color: white;" style="width:300px;height:40px">用户类型</span>
			<select class="layui-input-block" name="userType" style="width:300px;height:40px">
                <option value="waiter">服务人员</option>
                <option value="kitchen">后厨人员</option>
            </select>
		</div>

		<div style="margin-left:450px;margin-top:-320px;width:500px">
		<br>
		<div class="input-group" >
			<img class="layui-upload-img" id="demo1" src="${basePath }images/ozo.jpg" width="180" height="180">
		</div>
			<br>
		<button type="button" class="layui-btn" id="test1">
			<i class="layui-icon">&#xe67c;</i>上传图片
		</button>
		</div>
		<br>
		<button class="layui-btn layui-btn-lg" lay-submit lay-filter="addUserBut" style="width:200px;height:50px;margin-left: 280px;margin-top: 60px;">提  交</button>
	</form>

</div>
</div>

<script src="${basePath }layui/layui.js"></script>
<script>
	layui.use('upload', function(){
		var $ = layui.jquery,upload = layui.upload;
		//普通图片上传
		var uploadInst = upload.render({
			elem: '#test1',
			url: '${basePath }user/upload.do',
			before: function(obj){
				//预读本地文件示例，不支持ie8
				obj.preview(function(index, file, result){
					$('#demo1').attr('src', result); //图片链接（base64）
				});
			},
			done: function(res){
				//如果上传失败
				if(res.code > 0){
					return layer.msg('上传失败');
				}
				//上传成功
				layer.msg("上传成功");
			}
		});

		layui.use('form', function(){
			var form = layui.form;
			form.verify({
				confirmPass:function(value){
					if($('input[name=userPassword]').val() !== value)
						return '两次密码输入不一致！';
				}
			});

			//监听提交
			form.on('submit(addUserBut)', function(data) {
				console.log(data);
			});
			/*//监听提交
			form.on('submit(addUserBut)', function(data) {

				$.ajax({
					type: 'post',
					url: '${basePath }user/doadduser.do?descPath=${descPath}',
					success: function(data){
						console.log(data);
					}

				});
			});*/
		});
	});
</script>


</body>
</html>