<%--
  Created by IntelliJ IDEA.
  User: 胡中宝
  Date: 2020/7/26
  Time: 18:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>
<html>
<head>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
        <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
        <meta name="renderer" content="webkit">
        <!--国产浏览器高速模式-->
        <meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
        <title>登录</title>

        <!-- 公共样式 开始 -->
        <link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
        <link rel="bookmark" href="${basePath }images/ozo.jpg"/>
        <link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
        <link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
        <script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
        <!--[if lt IE 9]>
        <script src="${basePath }framework/html5shiv.min.js"></script>
        <script src="${basePath }framework/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="${basePath }layui/layui.js"></script>
        <!-- 滚动条插件 -->
        <link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
        <script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
        <script src="${basePath }framework/jquery.mousewheel.min.js"></script>
        <script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
        <script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
        <!-- 公共样式 结束 -->

    </head>
</head>
<body style="background-image:url(${basePath }images/222.jpg) ;background-size: cover;">
<div id="panel_login" style="width: 1000px;">
    <div class="layui-form1" style="padding: 80px 400px 10px;">
        <form class="layui-form" role="form" action="${basePath }food/addFood" method="post">
            <div class="input-group">
                <span class="input-group-addon" style="color: white;">菜品名称</span>
                <input type="text" style="width:300px;height:40px" name="dishesName" required lay-verify="dishesName" class="layui-input" autocomplete="off" placeholder="请输入菜品名">
            </div></br>
            <div class="input-group">
<%--                <input type="text" name="dishesIntroduction" lay-verify="required" autocomplete="off" class="layui-input"/><br/>--%>
                <span class="input-group-addon" style="color: white;">菜品简介</span>
                <input type="text" style="width:300px;height:40px" name="dishesIntroduction" required lay-verify="dishesIntroduction" autocomplete="off" class="layui-input" placeholder="请输入菜品简介">
            </div></br>
            <div class="input-group">
<%--                <input type="text" name="dishesPrice" lay-verify="required" autocomplete="off" class="layui-input"/><br/>--%>
                <span class="input-group-addon" style="color: white;">菜品价格</span>
                <input type="text" style="width:300px;height:40px" name="dishesPrice" required lay-verify="dishesIntroduction" autocomplete="off" class="layui-input" placeholder="请输入菜品价格">
                <span class="input-group-addon" style="color: white;">元</span>
            </div></br>
            <div class="input-group">
<%--                <input type="text" name="dishesRecommend" lay-verify="required" autocomplete="off" class="layui-input"/>颗星<br/>--%>
                <span class="input-group-addon" style="color: white;">推荐指数</span>
                <input type="text" style="width:300px;height:40px" name="dishesRecommend" required lay-verify="dishesRecommend" autocomplete="off" class="layui-input" placeholder="请输入推荐指数（1-5）">颗星
            </div>

                <%--<label class="layui-form-label" style="color: white">菜品类型</label>--%>
<%--                <span class="input-group-addon" style="color: white;" style="width:300px;height:40px">菜品类型</span>--%>

            <div class="layui-input-block" style="margin-left: 0px">
                <span class="input-group-addon" style="color: white;" style="width:300px;height:40px">菜品类型</span>
                <select class="foodType" id="foodType" name="dishesTypeDO.dishesTypeName" required lay-verify="required" autocomplete="off" class="layui-input" style="width:300px;height:40px">
                    <option value="" >请选择种类</option>
                    <c:forEach items="${foodtype}" var="dishesTypeDO">
                        <option value="${dishesTypeDO.dishesTypeId}">${dishesTypeDO.dishesTypeName}</option>
                    </c:forEach>
                </select>
            </div>
            <div>
                <%--<input type="submit" value="添加" style="width:200px;height:50px;margin-left: 40px;margin-top: 50px"/>--%>
                    <button class="layui-btn layui-btn-lg" lay-submit lay-filter="submitBut" style="width:200px;height:50px;margin-left: 80px;margin-top: 35px;">提  交</button>
            </div>
        </form>
    </div>
</div>

</body>
</html>