<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>

	<head>
        <meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<title>点餐系统</title>

		<!-- 公共样式 开始 -->
		<link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
		<link rel="bookmark" href="${basePath }images/favicon.ico"/>
		<link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
		<link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
		<script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
		<link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
	    <!--[if lt IE 9]>
	      	<script src="${basePath }framework/html5shiv.min.js"></script>
	      	<script src="${basePath }framework/respond.min.js"></script>
	    <![endif]-->
		<script type="text/javascript" src="${basePath }layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
		<script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
		<script src="${basePath }framework/jquery.mousewheel.min.js"></script>
		<script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->
		<script type="text/javascript" src="${basePath }js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="${basePath }css/bootstrap.min.css"><%--
		<link rel="stylesheet" type="text/css" href="${basePath }css/frameStyle.css">
		<script type="text/javascript" src="${basePath }framework/frame.js" ></script>--%>
	</head>

	<body>
	<input type="hidden" name="waiterName" value="${param.waiterName }"/>
	<input type="hidden" name="waiterId" value="${param.waiterId }"/>
		<%--<div class="cBody">--%>
			<div class="console">
				<form class="layui-form" action="${basePath }waiter/employeelist.do">
					<div class="layui-form-item">
						<div class="layui-input-inline">
							<input type="text" name="waiterName" id="demoReload" placeholder="输入员工姓名" autocomplete="off" class="layui-input">

						</div>
						<button class="layui-btn" lay-submit lay-filter="searchwaiter">查询</button>
					</div>
				</form>

				<script type="text/javascript">
					$(function(){
						$("a").click(function(){
							var serializeVal = $(":hidden").serialize();
							var href = this.href + "&" + serializeVal;
							window.location.href = href;
							return false;
						});
					})
				</script>

				<script>
					layui.use('form', function() {
						var form = layui.form;
				
						//监听提交
						form.on('submit(searchwaiter)', function(data) {
							console.log(data);
							/*layer.msg(JSON.stringify(data.field));
							return false;*/
						});
					});
				</script>


			<table class="table table-striped table-bordered table-hover" id="waitertable">
				<thead>
					<tr>
						<th>员工工号</th>
						<th>用户姓名</th>
						<th>员工类型</th>
						<th>员工姓名</th>
						<th>员工性别</th>
						<th>员工年龄</th>
						<th>电话号码</th>
						<th>入职时间</th>
						<th>基本操作</th>
					</tr>
				</thead>
				<c:forEach items="${waiters }" var="k">

					<tr>
						<td id="waiterid" >${k.waiterId }</td>
						<td>${k.allUserDO.userName }</td>
						<td>${k.allUserDO.userType == 'kitchen'?'后厨':(k.allUserDO.userType == 'waiter'?'服务员':'')}</td>
						<td>${k.waiterName }</td>
						<td>${k.waiterSex == '1'?'男':(k.waiterSex == '2'?'女':'')}</td>
						<td>${k.waiterAge }</td>
						<td>${k.waiterPhonenum }</td>
						<td>${k.waiterEntryTime }</td>
						<td>
							<a href="${basePath }waiter/update.do?id=${k.waiterId }"><button class="layui-btn layui-btn-xs" >修改</button></a>
							<a href="${basePath }waiter/delete.do?id=${k.waiterId }" onclick="confirmDel()"><button class="layui-btn layui-btn-xs" >删除</button></a>
							<script type="text/javascript">
								function confirmDel()
								{
									if(window.confirm("确定删除这条记录吗？")){
										document.location="${basePath }waiter/delete.do?id=${k.waiterId }"
									}
								}
							</script>

						</td>
					</tr>
				</c:forEach>
			</table>

				<%-- maxIndexPages:显示最多的分页数/次  maxPageItems:显示最多的纪录数/页--%>
			<pg:pager items="${total }" url="${basePath }waiter/employeelist.do" maxIndexPages="7" maxPageItems="5" scope="request">
				<pg:param name="kName" value="${kName }"/>
				<%@include file="./pager/pager_tag.jsp"%>
			</pg:pager>
			</div>


				<script>
                 //删除
			function delCustomList(_this){
				layui.use(['form','laydate'], function() {
					layer.confirm('确定要删除么？', {
						btn: ['确定', '取消'] //按钮
					}, function() {
						$(_this).parent().parent().remove();
						layer.msg('删除成功', {
							icon: 1
						});
					}, function() {
						layer.msg('取消删除', {
							time: 2000 //20s后自动关闭
						});
					});
				});
			}


			</script>

	</body>

</html>