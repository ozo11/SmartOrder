<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Title</title>
    <style type="text/css">
        * {font-size:12px;margin:0;}
        body {background:#fff;}
        form {margin:12px;}
        input.file{
            vertical-align:middle;
            position:relative;
            left:-218px;
            filter:alpha(opacity=0);
            opacity:0;
            z-index:1;
            *width:223px;
        }

        form input.viewfile {
            z-index:99;
            border:1px solid #ccc;
            padding:2px;
            width:150px;
            vertical-align:middle;
            color:#999;
        }

        form p span {
            float:left;
        }

        form label.bottom {
            border:1px solid #38597a;
            background:#4e7ba9;
            color:#fff;
            height:19px;
            line-height:19px;
            display:block;
            width:60px;
            text-align:center;
            cursor:pointer;
            float:left;
            position:relative;
            *top:1px;
        }

        form input.submit {
            border:0;
            background:#380;
            width:70px;
            height:22px;
            line-height:22px;
            color:#fff;
            cursor:pointer;
        }

        p.clear {
            clear:left;
            margin-top:12px;
        }

    </style>

</head>
<body>
<form action="${basePath }food/uploads?id=${dishesDo.dishesId}" method="post" enctype="multipart/form-data">
<%--    <p>--%>
<%--        <span>--%>
<%--            <label for ="viewfile">上传文件：</label>--%>
<%--            <input type="text" name="viewfile" id="viewfile" class="viewfile"/>--%>
<%--        </span>--%>
<%--        <label for="viewfile" class="bottom">查找文件</label><input type="file" size="27" name="file" οnchange="document.getElementById('viewfile').value=this.value;" class="file"/>--%>
<%--    </p>--%>
<%--    <p class="clear"><input class="submit" type="submit" value="确定上传" /></p>--%>
    <input type="file" name="file" multiple="multiple"/>(按住ctrl可选择多个文件)
    <input type="submit" value="提交"/>
</form>
<c:forEach items="${list}" var="dishesImage">
    <img src="${basePath }uploadMultipal\/${dishesImage.dishesImageName}" width="250px" length="250px" >
    <%--${dishesImage.dishesImageName}--%>
</c:forEach>
</body>
</html>
