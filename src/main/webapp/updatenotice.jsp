<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.*"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>
<%
    Date d = new Date();
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    String now = df.format(d);
%>

<html>
<head>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
        <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
        <meta name="renderer" content="webkit">
        <title>添加公告</title>

        <!-- 公共样式 开始 -->
        <link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
        <link rel="bookmark" href="${basePath }images/ozo.jpg"/>
        <link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
        <link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
        <script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
        <!--[if lt IE 9]>
        <script src="${basePath }framework/html5shiv.min.js"></script>
        <script src="${basePath }framework/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="${basePath }layui/layui.js"></script>
        <!-- 滚动条插件 -->
        <link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
        <script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
        <script src="${basePath }framework/jquery.mousewheel.min.js"></script>
        <script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
        <script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
        <!-- 公共样式 结束 -->

    </head>
</head>
<body style="background-image:url(${basePath }images/222.jpg) ;background-size: cover;">
<br/>
<div id="panel_login" style="width: 700px;">
    <div class="layui-form1" style="padding: 60px 320px 10px;">
<form id="addForm" class="layui-form" action="${basePath }notice/updateNotice2.do?noticeid=${NoticeDO.noticeId}" method="post">
    <div class="layui-form-item">
        <span class="input-group-addon" style="color: white;">管理员ID</span>
            <input type="text" placeholder="${NoticeDO.adminId}" value="${NoticeDO.adminId}" name="adminId" required lay-verify="required" autocomplete="off" class="layui-input" style="width:300px;height:40px" readonly>

    </div>
    <div class="layui-form-item">
        <span class="input-group-addon" style="color: white;">公告内容</span>
            <input type="text" placeholder="${NoticeDO.noticeContent}" value="${NoticeDO.noticeContent}" name="noticeContent" required lay-verify="required" autocomplete="off" class="layui-input" style="width:300px;height:40px">

    </div>
    <div class="layui-form-item">
        <span class="input-group-addon" style="color: white;">设置时间</span>
            <input type="date" placeholder="${NoticeDO.noticeTime}" name="noticeTime" autocomplete="off" class="layui-input" style="width:300px;height:40px" value="<%=now%>" readonly>

    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn layui-btn-lg" lay-submit lay-filter="submitBut" style="width:200px;height:50px;margin-left: -60px;margin-top: 30px;">确认更新</button>
        </div>
    </div>
</form>
    </div>
</div>

<script>
    layui.use(['upload','form'], function() {
        var form = layui.form;
        var upload = layui.upload;
        var layer = layui.layer;
        //监听提交
        //解决了layui.open弹窗从内部关闭这个弹窗的问题
        form.on('submit(submitBut)', function(data) {
            var updateFrame = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
            parent.layer.close(updateFrame);  //再改变当前层的标题
        });
        form.verify({
            //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
            ZHCheck: [
                /^[\u0391-\uFFE5]+$/
                ,'只允许输入中文'
            ]
        });
    });
</script>
</body>
</html>