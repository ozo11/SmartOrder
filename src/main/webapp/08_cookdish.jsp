<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>

<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <title>备菜信息</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
    <link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
    <script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
    <script type="text/javascript" src="${basePath }layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
    <script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
    <script src="${basePath }framework/jquery.mousewheel.min.js"></script>
    <script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <script type="text/javascript" src="${basePath }js/jquery-2.1.4.js"></script>
    <script type="text/javascript" src="${basePath }js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="${basePath }css/bootstrap.min.css">

</head>

<body>


<div >
    <div >

            <form class="layui-table" >
                <table class="table table-striped table-bordered table-hover" style="font-size: 12px; table-layout:fixed " width="700">
                    <tr>
                        <th >菜单单号</th>
                        <th >桌号</th>
                        <th >菜品编号</th>
                        <th >菜名</th>
                        <th >数量</th>
                        <th >开始时间</th>
                        <th >服务员号</th>
                        <th >烹饪状态</th>

                    </tr>

                    <c:forEach items="${orderDOS1}" var="orders">
                        <tr>
                            <td>${orders.dishesOrderId}</td>
                            <td>${orders.tableNum}</td>
                            <td>${orders.dishesDO.dishesId}</td>
                            <td>${orders.dishesDO.dishesName}</td>
                            <td>${orders.dishesAmount}</td>
                            <td>${orders.dishesStartTime}</td>
                            <td>${orders.waiterDO.waiterId}</td>
                            <td>${orders.dishesStatus}</td>

                        </tr>
                    </c:forEach>
                </table>

            </form>
        </div>
    </div>

</body>


</html>