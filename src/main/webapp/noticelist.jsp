<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <title>历史公告</title>

    <!-- 公共样式 开始 -->
    <link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
    <link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
    <script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
    <script type="text/javascript" src="${basePath }layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
    <script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
    <script src="${basePath }framework/jquery.mousewheel.min.js"></script>
    <script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="${basePath }framework/cframe.js"></script>
    <!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <script src="${basePath }js/out_intoData.js"></script>

</head>

<body>
<div class="cBody">
    <div class="layui-tab" lay-filter="myPage">
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <table class="layui-table">
                    <tr>
                        <th>公告ID</th>
                        <th>发布管理员ID</th>
                        <th>发布时间</th>
                        <th>公告内容</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                    <c:forEach items="${noticeList}" var="NoticeDO">
                        <tr>
                            <td>${NoticeDO.noticeId}</td>
                            <td>${NoticeDO.adminId}</td>
                            <td>${NoticeDO.noticeTime}</td>
                            <td>${NoticeDO.noticeContent}</td>
                            <td>
                                <a href="${basePath }notice/updateNotice?noticeid=${NoticeDO.noticeId}"><button class="layui-btn layui-btn-xs" >修改</button></a>
                                <a href="${basePath }notice/deleteNotice?noticeid=${NoticeDO.noticeId}" onclick="confirmDel()"><button class="layui-btn layui-btn-xs" >删除</button></a>
                                <script type="text/javascript">
                                    function confirmDel()
                                    {
                                        if(window.confirm("确定删除这条记录吗？")){
                                            document.location="${basePath }notice/deleteNotice?noticeid=${NoticeDO.noticeId}"
                                        }
                                    }
                                </script>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

                <pg:pager items="${total}" url="${basePath}notice/getNotice" maxIndexPages="7" maxPageItems="5" scope="request">
                    <pg:param name="orderId" value="${orderId}"></pg:param>
                    <%@include file="./pager/pager_tag.jsp"%>
                </pg:pager>

                <!-- layUI 分页模块 -->
                <div id="pages1"></div>
                <%--<script>
                    layui.use(['laypage', 'layer'], function() {
                        var laypage = layui.laypage,
                            layer = layui.layer;

                        //总页数大于页码总数
                        laypage.render({
                            elem: 'pages1',
                            count: 30,
                            layout: ['prev', 'page', 'next', 'limit', 'skip'],
                            jump: function(obj) {
                                console.log(obj)
                            }
                        });
                    });
                </script>--%>
            </div>

            </div>
        </div>
    </div>
    <script>
        layui.use('element', function() {
            var element = layui.element;

            //获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
            var layid = location.hash.replace(/^#test1=/, '');
            element.tabChange('myPage', layid); //假设当前地址为：http://a.com#test1=222，那么选项卡会自动切换到“发送消息”这一项

            //监听Tab切换，以改变地址hash值
            element.on('tab(myPage)', function() {
                location.hash = 'test1=' + this.getAttribute('lay-id');
                console.log(this.getAttribute('lay-id'));
            });
        });
        //删除
        function delCustomList(_this){
            layui.use(['form','laydate'], function() {
                layer.confirm('确定要删除么？', {
                    btn: ['确定', '取消'] //按钮
                }, function() {
                    $(_this).parent().parent().remove();
                    layer.msg('删除成功', {
                        icon: 1
                    });
                }, function() {
                    layer.msg('取消删除', {
                        time: 2000 //20s后自动关闭
                    });
                });
            });
        }
    </script>
</div>
</body>

</html>