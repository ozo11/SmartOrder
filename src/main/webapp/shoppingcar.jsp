<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>
<html>
<head>
    <meta charset="utf-8">
    <title>购物车</title>
    <!--样式的设置-->
    <style type="text/css">
        body {
            margin: 0px;
            padding: 0px;
            font-size: 12px;
            line-height: 20px;
            color: #333;
        }

        ul,li {
            list-style: none;
            margin: 0px;
            padding: 0px;
        }

        a {
            color: #cd590c;
            text-decoration: none;
        }

        a:hover {
            color: #cd590c;
            text-decoration: underline;
        }

        img {
            border: 0px;
            vertical-align: middle;
        }

        #header {
            height: 40px;
            margin: 10px auto 10px auto;
            width: 800px;
            clear: both;
        }

        #nav {
            margin: 10px auto 10px auto;
            width: 800px;
            clear: both;
        }

        #navlist {
            width: 800px;
            margin: 0px auto 0px auto;
            height: 23px;
        }

        #navlist li {
            float: left;
            height: 23px;
            line-height: 26px;
        }

        .navlist_red_left {
            background-image: url(/img/register_bg.gif);
            background-repeat: no-repeat;
            background-position: -12px -92px;
            width: 3px;
        }

        .navlist_red {
            background-color: #ff6600;
            text-align: center;
            font-size: 14px;
            font-weight: bold;
            color: #FFF;
            width: 130px;
        }

        .navlist_red_arrow {
            background-color: #ff6600;
            background-image: url(img/register_bg.gif);
            background-repeat: no-repeat;
            background-position: 0px 0px;
            width: 13px;
        }

        .navlist_gray {
            background-color: #e4e4e4;
            text-align: center;
            font-size: 14px;
            font-weight: bold;
            width: 150px;
        }

        .navlist_gray_arrow {
            background-color: #e4e4e4;
            background-image: url(img/register_bg.gif);
            background-repeat: no-repeat;
            background-position: 0px 0px;
            width: 13px;
        }

        .navlist_gray_right {
            background-image: url(img/register_bg.gif);
            background-repeat: no-repeat;
            background-position: -12px -138px;
            width: 3px;
        }

        #content {
            width: 800px;
            margin: 10px auto 5px auto;
            clear: both;
        }

        .title_1 {
            text-align: center;
            width: 50px;
        }

        .title_2 {
            text-align: center;
        }

        .title_3 {
            text-align: center;
            width: 80px;
        }

        .title_4 {
            text-align: center;
            width: 80px;
        }

        .title_5 {
            text-align: center;
            width: 100px;
        }

        .title_6 {
            text-align: center;
            width: 80px;
        }

        .title_7 {
            text-align: center;
            width: 60px;
        }

        .line {
            background-color: black;
            height: 3px;
        }

        .shopInfo {
            padding-left: 10px;
            height: 35px;
            vertical-align: bottom;
        }

        .num_input {
            border: solid 1px #666;
            width: 25px;
            height: 15px;
            text-align: center;
        }

        .td_1,
        .td_2,
        .td_3,
        .td_4,
        .td_5,
        .td_6,
        .td_7,
        .td_8 {
            background-color: white;
            border-bottom: solid 1px white;
            border-top: solid 1px black;
            text-align: center;
            padding: 5px;
        }

        .td_1,
        .td_3,
        .td_4,
        .td_5,
        .td_6,
        .td_7 {
            border-right: solid 1px #FFF;
        }

        .td_3 {
            /*text-align: left;*/
            font-weight: bold;
        }

        .td_4 {
            font-weight: bold;
        }

        .td_7 {
            font-weight: bold;
            color: #fe6400;
            font-size: 14px;
        }

        .hand {
            cursor: pointer;
        }

        .shopend {
            text-align: right;
            padding-right: 10px;
            padding-bottom: 10px;
        }

        .yellow {
            font-weight: bold;
            color: #FE6400;
            font-size: 18px;
        }
    </style>
    <!--  用Jquery 实现相应功能-->
    <script type="text/javascript" src="${basePath}js/jquery.min.js"></script>
    <script type="text/javascript">
        $(function() {
            //点击复选框全选或不全选效果
            $("#allCheckBok").click(function() {
                var checked = $(this).is(":checked");
                $(".td_1").children().attr("checked", checked);
            });

            //判断是否全选
            function Allchk() {
                var checkedB = $(".td_1").children();
                var sum = checkedB.size();
                var k = 0;
                checkedB.each(function(index, dom) {
                    if($(dom).is(":checked"))
                        k++;
                });

                if(k == sum) {
                    $("#allCheckBok").attr("checked", true);
                } else {
                    $("#allCheckBok").attr("checked", false)
                }

            }

            Allchk(); //页面加载完后运行

            //单选判断
            $(".td_1").children().click(function() {
                Allchk();
            })

            $("#all").click(function () {
                var $tr = $("#shopping").find("tr[id]");
                var summer = 0;
                var integral = 0;
                var dishesnum = 0;
                var array = new Array();
                var arrayId= new Array();
                var tableNumber = $("#tableNum").val();
                $tr.each(function(i, dom) {
                    var num = $(dom).children(".td_6").find("input").val(); //商品数量
                    var Id = $(dom).children(".td_9").find("input").val();//商品编号
                    // tableNumber = $(dom).children(".td_10").find("input").val();//桌号
                    if (num != 0){
                        var test = $(this).closest("tr").find('input[name="dishesId"]').val();
                        // alert(test)
                        array.push(num);
                        arrayId.push(test);
                    }
                    //alert(array);
                     // alert(arrayId);
                    var price = num * $(dom).children(".td_5").text(); //商品小计
                    $(dom).children(".td_7").html(price); //显示商品小计
                    summer += price; //总价
                    integral += $(dom).children(".td_4").text() * num; //积分
                    dishesnum +=num;
                });
                $("#total").text(summer);
                $("#integral").text(integral);
                $("#foodnum").text(dishesnum);
                location.href="${basePath }order/findOrder?array="+array+"&summer="+summer+"&arrayId="+arrayId+"&tableNumber="+tableNumber+"&waiterId="+${loginUser.userId};
                $.ajax({
                    url:"${basePath }order/findAllFood",
                    type:"GET",
                    data:{
                        "array":array,
                         "time" : new Date().getTime()
                    },
                    traditional:true,
                });
            });
            function doGetCheckedIds() {
                var array = [];
                $("#body imput[name=foodnum]");
                each(function () {
                    if ($(this).prop("checked")){
                        array.push($(this).val());
                    }
                });
                return array;
                alert("1");
            }
            //计算总价与小计
            function prodC() {
                var $tr = $("#shopping").find("tr[id]");
                var summer = 0;
                var integral = 0;
                var dishesnum = 0;
                $tr.each(function(i, dom) {
                    var num = $(dom).children(".td_6").find("input").val(); //商品数量
                    var price = num * $(dom).children(".td_5").text(); //商品小计
                    $(dom).children(".td_7").html(price); //显示商品小计
                    summer += price; //总价
                    integral += $(dom).children(".td_4").text() * num; //积分
                    dishesnum +=num;
                });

                $("#total").text(summer);
                $("#integral").text(integral);
                $("#foodnum").text(dishesnum);
                doGetCheckedIds()
            }
            // prodC(); //页面加载完成后运行
            //商品增加减少 ，flag 为true时增加 flag为false时减少
            function changN(dom, flag) {
                var $input = $(dom).parent().find("input");
                var value = $input.val();
                if(flag) {
                    value++;
                } else {
                    value--;
                    if(value < 0) {
                        value = 0;
                        alert("菜品数量不能为负值")
                    }
                }
                $input.val(value);
                prodC();
            };

            //点击增加
            $(".td_6").find("img[alt='minus']").click(function() {
                changN(this, false);
            });
            //点击减少
            $(".td_6").find("img[alt='add']").click(function() {
                changN(this, true);
            });

            //点击删除
            $(".td_8").find("a").click(function() {
                $(this).parent().parent().prev().remove(); //删除前一tr
                $(this).parent().parent().remove(); //删除当前tr
                prodC();
            });
            //点击删除所选
            $("#deleteAll").click(function() {
                $("#shopping").find("tr[id]").each(function(i, e) {
                    var $tr = $(e);
                    var checked = $tr.children(".td_1").children().is(":checked");
                    if(checked) {
                        $tr.prev().remove();
                        $tr.remove();
                    }
                });
                prodC()

            });

        })
    </script>

        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
        <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
        <meta name="renderer" content="webkit">
        <!--国产浏览器高速模式-->
        <meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
        <title>点餐</title>

        <!-- 公共样式 开始 -->
        <link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
        <link rel="bookmark" href="${basePath }images/ozo.jpg"/>
        <link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
        <link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
        <script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
        <link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
        <!--[if lt IE 9]>
        <script src="${basePath }framework/html5shiv.min.js"></script>
        <script src="${basePath }framework/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="${basePath }layui/layui.js"></script>
        <!-- 滚动条插件 -->
        <link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
        <script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
        <script src="${basePath }framework/jquery.mousewheel.min.js"></script>
        <script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
        <script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->

</head>
<body>
<body style="background-color: white;">
<div id="nav">您的位置：
    <a href="#">首页</a>>
    <a href="#">点餐</a></div>
<div id="content">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="shopping">
            <input type="hidden" name="loginuser" value="${loginUser.userId }"/>
            <tr>
                <%--<td class="title_1"><input id="allCheckBok" type="checkbox" value="">全选</td>--%>
                <td class="title_2">图片</td>
                <td class="title_2">菜品</td>
                <td class="title_3">简介</td>
                <td class="title_4">单价(元)</td>
                <td class="title_5">数量</td>
                <td class="title_6">小计(元)</td>
                <%--<td class="title_7">操作</td>--%>
            </tr>

            <tr>
                <td colspan="8" class="line"></td>
            </tr>
            <c:forEach items="${list}" var="dishes">
            <tr id="product1">
                <%--<td class="td_1"><input type="checkbox" name="cartCheckBox" value="product1"></td>--%>
                <td class="td_2"><img src="${basePath}tubiao/yu.png" alt="shopping"></td>
                <td class="td_3">
                    ${dishes.dishesName}</td>
                    <%--<br>简介：${dishes.dishesIntroduction}--%>
                <td class="td_4">${dishes.dishesIntroduction}</td>
                <td class="td_5">${dishes.dishesPrice}</td>
                <td class="td_6"><img src="${basePath}tubiao/jian.png" alt="minus" class="hand"> <input type="text" id="foodnum" name="foodnum" value="0" class="num_input"> <img src="${basePath}tubiao/add.png" alt="add" class="hand" /></td>
                <td class="td_7"></td>
                <%--<td class="td_8">
                    <a href="javascript:void(0);">删除</a>
                </td>--%>
                <td class="td_9"><input  type="hidden" value="${dishes.dishesId}" name="dishesId" id="dishesId"/></td>
            </tr><br/>
            </c:forEach>
            <br>
            <%--<tr>
                <td class="td_10">
                    <br>

                </td>

                <br>
                <input type="text" name="tableNum" id="tableNum" style="width:300px;height:40px" required
                       layui-filter="required" autocomplete="off" class="layui-input" placeholder="请输入桌号"/>
                <td colspan="5" class="shopend">
                    <label >总金额：</label>
                    <label id="total" class="yellow"></label>元
                    <br >
                    &lt;%&ndash;<input name="" type="submit" src="${basePath}tubiao/tijiao.png" id = "all"/>&ndash;%&gt;
                    <br>
                    <button class="layui-btn" lay-submit id="all">立即下单</button>
                </td>
            </tr>--%>
        </table>
    <form>
        <div style="margin-left: 650px">

            <div>
                <label >总金额：</label>
                <label id="total" class="yellow"></label>元
            </div>
            <div>
                <input type="text" name="tableNum" id="tableNum" style="width:100px;height:40px;margin-top: 10px" required
               layui-filter="required" autocomplete="off" class="layui-input" placeholder="请输入桌号"/>
            </div>
            <div>
                <button class="layui-btn layui-btn-lg" lay-submit id="all" style="margin-top: 10px">立即下单</button>
            </div>

        </div>

    </form>
</div>

</body>
</body>
</html>
