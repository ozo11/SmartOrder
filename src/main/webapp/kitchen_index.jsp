<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>

	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<title>点餐系统</title>

		<!-- 公共样式 开始 -->
		<link rel="shortcut icon" href="${basePath }images/login_bg2.jpg"/>
		<link rel="bookmark" href="${basePath }images/login_bg2.jpg"/>
		<link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
		<link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
		<script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
		<link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
	    <!--[if lt IE 9]>
	      	<script src="${basePath }framework/html5shiv.min.js"></script>
	      	<script src="${basePath }framework/respond.min.js"></script>
	    <![endif]-->
		<script type="text/javascript" src="${basePath }layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
		<script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
		<script src="${basePath }framework/jquery.mousewheel.min.js"></script>
		<script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->
		
		<link rel="stylesheet" type="text/css" href="${basePath }css/frameStyle.css">
		<script type="text/javascript" src="${basePath }framework/frame.js" ></script>
		
	</head>

	<body>
		<!-- 左侧菜单 - 开始 -->
		<div class="frameMenu" style="background-image:url(images/222.jpg) ;background-size: cover;">
		    <div class="logo">
		        <img src="${basePath }images/login_bg2.jpg"/>
		        <div class="logoText">
		            <h1>点餐系统</h1>
		            
		        </div>
		    </div>
		    <div class="menu">
		        <ul>
		        	
		        	<li>
		                <a class="menuFA" href="javascript:void(0)"><i class="iconfont icon-zhishi left"></i>后厨管理<i class="iconfont icon-dajiantouyou right"></i></a>
		                <dl>

		                	<dt><a href="javascript:void(0)" onclick="menuCAClick('${basePath }DishesOrder/list',this)">备菜信息</a></dt>
							<dt><a href="javascript:void(0)" onclick="menuCAClick('${basePath }DishesOrder/cookedlist',this)">完成菜单</a></dt>
							<dt><a href="javascript:void(0)" onclick="menuCAClick('${basePath }notice/checkNotice',this)">查看公告</a></dt>

		                </dl>
		           	</li>
		        	
		        </ul>
		    </div>
		</div>
		<!-- 左侧菜单 - 结束 -->
		
		<div class="main">
			<!-- 头部栏 - 开始 -->
			<div class="frameTop" style="background-image:url(images/222.jpg) ;background-size: cover;">
				<img class="jt" src="${basePath }images/top_jt.png"/>
				<div class="topMenu" >
					<ul>

						<li style="background-color:rgb(255, 255, 255);"><a href="javascript:void(0)" onclick="menuCAClick('${basePath }modify_password.jsp',this)"><i class="iconfont icon-yonghu1"></i>后厨人员001</a></li>
						<%--<li style="background-color:rgb(9, 4, 78);"><a href="javascript:void(0)" onclick="menuCAClick('${basePath }modify_password.jsp',this)"><i class="iconfont icon-xiugaimima"></i>修改密码</a></li>--%>
						<li style="background-color:rgb(255, 255, 255);"><a href="${basePath }user/login"><i class="iconfont icon-084tuichu"></i>退出</a></li>
					</ul>
				</div>
			</div>
			<!-- 头部栏 - 结束 -->
			

			<!-- 核心区域 - 开始 -->
			<div class="frameMain" >
				<div class="title" id="frameMainTitle" style="background-image:url(images/222.jpg) ;background-size: cover;">
					<span><i class="iconfont icon-xianshiqi"></i>后台首页</span>
				</div>
				<div class="con">
					<iframe id="mainIframe" src="${basePath }index.jsp" scrolling="no"></iframe>
				</div>
			</div>
			<!-- 核心区域 - 结束 -->
		</div>
	</body>


</html>