<%@ page language="java" contentType="text/html; charset=UTF-8"
pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
+ path + "/";
session.setAttribute("basePath", basePath);
%>
<html>

	<head>
        <meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
		<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
		<meta name="renderer" content="webkit">
		<title>点餐系统</title>

		<!-- 公共样式 开始 -->
		<link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
		<link rel="bookmark" href="${basePath }images/favicon.ico"/>
		<link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
		<link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
		<script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
		<link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">
	    <!--[if lt IE 9]>
	      	<script src="${basePath }framework/html5shiv.min.js"></script>
	      	<script src="${basePath }framework/respond.min.js"></script>
	    <![endif]-->
		<script type="text/javascript" src="${basePath }layui/layui.js"></script>
		<!-- 滚动条插件 -->
		<link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
		<script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
		<script src="${basePath }framework/jquery.mousewheel.min.js"></script>
		<script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
		<script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
		<!-- 公共样式 结束 -->
		<script type="text/javascript" src="${basePath }js/bootstrap.min.js"></script>
		<link rel="stylesheet" href="${basePath }css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="${basePath }css/frameStyle.css">
		<script type="text/javascript" src="${basePath }framework/frame.js" ></script>
	</head>

	<body>
	<input type="hidden" name="kitchenName" value="${param.kitchenName }"/>
	<input type="hidden" name="kitchenId" value="${param.kitchenId }"/>
			<div class="console">
				<form class="layui-form" action="${basePath }kitchen/employeelist.do">
					<div class="layui-form-item">
						<div class="layui-input-inline">
							<input type="text" name="kitchenName" id="demoReload" placeholder="输入员工姓名" autocomplete="off" class="layui-input">

						</div>
						<button class="layui-btn" lay-submit lay-filter="searchkitchen">查询</button>
					</div>
				</form>

				<script type="text/javascript">
					$(function(){
						$("a").click(function(){
							var serializeVal = $(":hidden").serialize();
							var href = this.href + "&" + serializeVal;
							window.location.href = href;
							return false;
						});
					})
				</script>

				<script>
					layui.use('form', function() {
						var form = layui.form;
				
						//监听提交
						form.on('submit(searchkitchen)', function(data) {
							console.log(data);
							/*layer.msg(JSON.stringify(data.field));
							return false;*/
						});
					});
				</script>


			<table class="table table-striped table-bordered table-hover" id="kitchentable">
				<thead>
					<tr>
						<th>员工工号</th>
						<th>用户姓名</th>
						<th>员工类型</th>
						<th>员工姓名</th>
						<th>员工性别</th>
						<th>员工年龄</th>
						<th>电话号码</th>
						<th>入职时间</th>
						<th>基本操作</th>
					</tr>
				</thead>
				<c:forEach items="${kitchens }" var="k">

					<tr>
						<td id="kitchenid" >${k.kitchenId }</td>
						<td>${k.allUserDO.userName }</td>
						<td>${k.allUserDO.userType == 'kitchen'?'后厨':(k.allUserDO.userType == 'waiter'?'服务员':'')}</td>
						<td>${k.kitchenName }</td>
						<td>${k.kitchenSex == '1'?'男':(k.kitchenSex == '2'?'女':'')}</td>
						<td>${k.kitchenAge }</td>
						<td>${k.kitchenPhonenum }</td>
						<td>${k.kitchenEntryTime }</td>
						<td>
							<%--<button class="layui-btn layui-btn-xs" href="${basePath }kitchen/update.do?id=${k.kitchenId }" &lt;%&ndash;onclick="updateBut()"&ndash;%&gt;>修改</button>
--%>
							<a href="${basePath }kitchen/update.do?id=${k.kitchenId }"><button class="layui-btn layui-btn-xs" >修改</button></a>
								<a href="${basePath }kitchen/delete.do?id=${k.kitchenId }" onclick="confirmDel()"><button class="layui-btn layui-btn-xs" >删除</button></a>
								<script type="text/javascript">
									function confirmDel()
									{
										if(window.confirm("确定删除这条记录吗？")){
											document.location="${basePath }kitchen/delete.do?id=${k.kitchenId }"
										}
									}
								</script>
							<%--<script>
								//修改按钮
								var updateFrame = null;
								function updateBut(){
									layui.use('layer', function() {
										var layer = layui.layer;
										//iframe层-父子操作
										updateFrame = layer.open({
											title: "员工信息修改",
											type: 2,
											area: ['70%', '70%'],
											scrollbar: false,	//默认：true,默认允许浏览器滚动，如果设定scrollbar: false，则屏蔽
											maxmin: true,
											content: '${basePath }kitchen/update.do?id=${k.kitchenId }',
											success: function (layero, index) {
												// 获取子页面的iframe
												var iframe = window['layui-layer-iframe' + index];
												//$("#kitchenId").val(data.kitchenId);
												// 向子页面的全局函数child传参
												iframe.child(data);
											}
										});

									});

								}
								</script>--%>
							<%--<button class="layui-btn layui-btn-xs" onclick="delCustomList()">删除</button>--%>
						</td>
					</tr>
				</c:forEach>
			</table>

				<%-- maxIndexPages:显示最多的分页数/次  maxPageItems:显示最多的纪录数/页--%>
			<pg:pager items="${total }" url="${basePath }kitchen/employeelist.do" maxIndexPages="7" maxPageItems="5" scope="request">
				<pg:param name="kName" value="${kName }"/>
				<%@include file="./pager/pager_tag.jsp"%>
			</pg:pager>
			</div>


				<script>
                 //删除
			function delCustomList(_this){
				layui.use(['form','laydate'], function() {
					layer.confirm('确定要删除么？', {
						btn: ['确定', '取消'] //按钮
					}, function() {
						$(_this).parent().parent().remove();
						layer.msg('删除成功', {
							icon: 1
						});
					}, function() {
						layer.msg('取消删除', {
							time: 2000 //20s后自动关闭
						});
					});
				});
			}


			</script>

	</body>

</html>