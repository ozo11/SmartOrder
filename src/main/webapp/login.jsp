<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <title>登录</title>

    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="${basePath }images/ozo.jpg"/>
    <link rel="bookmark" href="${basePath }images/ozo.jpg"/>
    <link rel="stylesheet" type="text/css" href="${basePath }css/base.css">
    <link rel="stylesheet" type="text/css" href="${basePath }css/iconfont.css">
    <script type="text/javascript" src="${basePath }framework/jquery-1.11.3.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="${basePath }layui/css/layui.css">

    <!--[if lt IE 9]>
    <script src="${basePath }framework/html5shiv.min.js"></script>
    <script src="${basePath }framework/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="${basePath }layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="${basePath }css/jquery.mCustomScrollbar.css">
    <script src="${basePath }framework/jquery-ui-1.10.4.min.js"></script>
    <script src="${basePath }framework/jquery.mousewheel.min.js"></script>
    <script src="${basePath }framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="${basePath }framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <link rel="stylesheet" type="text/css" href="${basePath }css/login.css">
    <script type="text/javascript" src="${basePath }js/login.js"></script>
</head>

<body>

<!--主体 开始-->
<div class="login_main">

    <!--轮播图 开始-->
    <div class="layui-carousel lbt" id="loginLbt">
        <div carousel-item>
            <div class="item" style="background: url(${basePath }images/bj2.jpg) no-repeat; background-size: cover;"></div>

        </div>
    </div>
    <!--轮播图 结束-->

    <div class="form_tzgg" style="margin-top: 100px; margin-left:500px;width: 500px;background-color:rgb(236, 235, 229);opacity: 0.9;">
        <div class="form" >
            <form action="${basePath }user/dologin.do" method="post" class="layui-form">
                <div class="title" >用户登录</div>
                <div class="con" onclick="getFocus(this)">
                    <input type="text" name="userNumber" required lay-verify="userNumber" placeholder="请输入您的用户账号" autocomplete="off" class="layui-input">
                </div>
                <br>
                <div class="con" onclick="getFocus(this)">
                    <input type="password" name="userPassword" required  lay-verify="userPassword" placeholder="请输入您的账户密码" autocomplete="off" class="layui-input">
                </div>
                <%--<div class="con" onclick="getFocus(this)">
                    <input type="text" name="checkCode"  lay-verify="checkCode" placeholder="请输入验证码" autocomplete="off" class="layui-input">
                </div>
                <div class="con" onclick="getFocus(this)">
                    <img id="checkCodeImg" οnclick="checkCode(this)"/>
                </div>--%>
                <br>
                <div class="layui-form-item">
                    <input type="radio" name="alluser" value="admin" title="管理员" checked lay-filter="userType">
                    <input type="radio" name="alluser" value="waiter" title="服务员" lay-filter="userType">
                    <input type="radio" name="alluser" value="kitchen" title="后厨人员" lay-filter="userType">
                </div>
                <br>
                <div class="but">
                    <button class="layui-btn layui-btn-fluid login_but" lay-submit lay-filter="loginBut">登 录</button>
                </div>

            </form>
        </div>
        <script>
            layui.use('form', function() {
                var form = layui.form;
                form.verify({
                    userNumber: function(value, item){ //value：表单的值、item：表单的DOM对象
                        if(value == null || value == ""){
                            return '请输入您的用户账号！';
                        }
                    },
                    userPassword: function(value, item){
                        if(value == null || value == ""){
                            return '请输入您的账户密码！';
                        }
                    }/*,
                    checkCode: function(value, item){
                        if(value == null || value == ""){
                            return '请输入验证码！';
                        }
                    }*/
                });
                //监听提交
                form.on('submit(loginBut)', function(data) {
                    //layer.msg(JSON.stringify(data.field));
                    console.log(data);
                    return true;
                });

                //监听用户类型，改变风格
                form.on('radio(userType)', function(data){
                    if(data.value == "admin"){
                        $(".login_but").css("cssText", "background-color:#ffd205 !important");
                    }
                    if(data.value == "waiter"){
                        $(".login_but").css("cssText", "background-color:#16c6f9 !important");
                    }
                    if(data.value == "kitchen"){
                        $(".login_but").css("cssText", "background-color:#57c201 !important");
                    }
                });
            });
        </script>

        <div class="tzgg">
            <div class="title">通知公告</div>
            <div class="con">
                <ul>
                    <li><span style="color: black;">07-19</span>请注意添加菜品</li>
                    <li><span style="color: black;">07-20</span>请注意添加员工</li>
                    <li><span style="color: black;">07-29</span>请注意订单</li>
                </ul>
            </div>
        </div>
    </div>

</div>
<!--主体 结束-->
</body>

</html>