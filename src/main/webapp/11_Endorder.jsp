<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
	session.setAttribute("basePath", basePath);
%>
<%@ taglib prefix="pg" uri="http://jsptags.com/tags/navigation/pager" %>
<html>

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
	<!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
	<meta name="renderer" content="webkit">
	<meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
	<title>订单信息</title>

	<!-- 公共样式 开始 -->
	<link rel="stylesheet" type="text/css" href="${basePath}css/base.css">
	<link rel="stylesheet" type="text/css" href="${basePath}css/iconfont.css">
	<script type="text/javascript" src="${basePath}framework/jquery-1.11.3.min.js"></script>
	<link rel="stylesheet" type="text/css" href="${basePath}layui/css/layui.css">
	<script type="text/javascript" src="${basePath}layui/layui.js"></script>
	<!-- 滚动条插件 -->
	<link rel="stylesheet" type="text/css" href="${basePath}css/jquery.mCustomScrollbar.css">
	<script src="${basePath}framework/jquery-ui-1.10.4.min.js"></script>
	<script src="${basePath}framework/jquery.mousewheel.min.js"></script>
	<script src="${basePath}framework/jquery.mCustomScrollbar.min.js"></script>
	<script src="${basePath}framework/cframe.js"></script>
	<!-- 仅供所有子页面使用 -->
	<!-- 公共样式 结束 -->
	<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
	<script type="text/javascript">
		// $(function () {
		// 	$("#EndList").click(function () {
		// 		location.href = "/adminOrder/findEndOrder"
		// 	});
		//     $("#NotEndList").click(function () {
		//         location.href = "/adminOrder/findNotEndOrder"
		//     });
		// })
	</script>
	<script src="${basePath}js/out_intoData.js"></script>
	<script type="text/javascript" src="${basePath}js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="${basePath}css/bootstrap.min.css">
</head>

<body>
<div class="cBody">
	<div class="layui-tab" lay-filter="myPage">
		<form class="layui-form" action="${basePath}adminOrder/findEndOrder" method="post">
			<div class="layui-form-item">
				<div class="layui-input-inline">
					<input type="text" name="tableNum" required lay-verify="required" placeholder="输入桌号" autocomplete="off" class="layui-input">
				</div>
				<div>
					<button class="layui-btn" lay-submit >查询</button>
				</div>
			</div>
		</form>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<table class="layui-table">
					<thead>
					<tr>
					<tr>
						<th>订单号</th>
						<th>桌号</th>
						<th>订单金额(元)</th>
						<th>创建时间</th>
						<th>完成时间</th>
						<th>订单商品信息</th>
						<th>订单状态</th>
						<th>服务人员姓名</th>
					</tr>
					</tr>
					</thead>
					<tbody>
					<c:forEach items="${list1}" var="order1">
						<tr>
							<td>${order1.orderId}</td>
							<td>${order1.tableNum}</td>
							<td>${order1.orderSum}</td>
							<td>${order1.orderCreateTime}</td>
							<td>${order1.orderEndTime}</td>
							<td>${order1.orderDetile}</td>
							<td>
								<c:if test="${order1.orderStatus==3}">
									<font color="blue">订单已完成</font>
								</c:if>
							</td>
							<td>${order1.waiterDO.waiterName}</td>
						</tr>
					</c:forEach>
					</tbody>
				</table>
				<%--<pg:pager items="${total}" url="/adminOrder/findNotEndOrder" maxIndexPages="7" maxPageItems="5" scope="request">
					<pg:param name="orderId" value="${orderId}"></pg:param>
					<%@include file="pager/pager_tag.jsp"%>
				</pg:pager>--%>
				<!-- layUI 分页模块 -->
				<div id="pages1"></div>
				<%--                <script>--%>
				<%--                    layui.use(['laypage', 'layer'], function() {--%>
				<%--                        var laypage = layui.laypage,--%>
				<%--                            layer = layui.layer;--%>

				<%--                        //总页数大于页码总数--%>
				<%--                        laypage.render({--%>
				<%--                            elem: 'pages1',--%>
				<%--                            count: ${total},--%>
				<%--                            layout: ['prev', 'page', 'next', 'limit', 'skip'],--%>
				<%--                            jump: function(obj) {--%>
				<%--                                console.log(obj)--%>
				<%--                            }--%>
				<%--                        });--%>
				<%--                    });--%>
				<%--                </script>--%>
			</div>
		</div>
	</div>
	<script>
		layui.use('element', function() {
			var element = layui.element;

			//获取hash来切换选项卡，假设当前地址的hash为lay-id对应的值
			var layid = location.hash.replace(/^#test1=/, '');
			element.tabChange('myPage', layid); //假设当前地址为：http://a.com#test1=222，那么选项卡会自动切换到“发送消息”这一项

			//监听Tab切换，以改变地址hash值
			element.on('tab(myPage)', function() {
				location.hash = 'test1=' + this.getAttribute('lay-id');
				console.log(this.getAttribute('lay-id'));
			});
		});
		//删除
		function delCustomList(_this){
			layui.use(['form','laydate'], function() {
				layer.confirm('确定要删除么？', {
					btn: ['确定', '取消'] //按钮
				}, function() {
					$(_this).parent().parent().remove();
					layer.msg('删除成功', {
						icon: 1
					});
				}, function() {
					layer.msg('取消删除', {
						time: 2000 //20s后自动关闭
					});
				});
			});
		}
	</script>
</div>
</body>
</html>