<%--
  Created by IntelliJ IDEA.
  User: 胡中宝
  Date: 2020/7/29
  Time: 17:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
            + path + "/";
    session.setAttribute("basePath", basePath);
%>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <title>结算列表</title>

    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="${basePath}images/login_bg2.ico"/>
    <link rel="bookmark" href="${basePath}images/login_bg2.ico"/>
    <link rel="stylesheet" type="text/css" href="${basePath}css/base.css">
    <link rel="stylesheet" type="text/css" href="${basePath}css/iconfont.css">
    <script type="text/javascript" src="${basePath}framework/jquery-1.11.3.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="${basePath}layui/css/layui.css">
    <!--[if lt IE 9]>
    <script src="${basePath}framework/html5shiv.min.js"></script>
    <script src="${basePath}framework/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="${basePath}layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="${basePath}css/jquery.mCustomScrollbar.css">
    <script src="${basePath}framework/jquery-ui-1.10.4.min.js"></script>
    <script src="${basePath}framework/jquery.mousewheel.min.js"></script>
    <script src="${basePath}framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="${basePath}framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <link rel="stylesheet" type="text/css" href="${basePath}css/frameStyle.css">
    <script type="text/javascript" src="${basePath}framework/frame.js" ></script>

    <style>
        .layui-table img {
            max-width: none;
        }
    </style>

</head>

<body>
<div class="cBody">
    <div class="console">
        <form class="layui-form" action="/order/selectOrder" method="post">
            <div class="layui-form-item">
                <div class="layui-input-inline">
                    <input type="text" name="id" id="id" required lay-verify="required" placeholder="输入桌号" autocomplete="off" class="layui-input">
                </div>
                <button class="layui-btn" lay-submit lay-filter="searchkitchen">查询</button>
<%--                <button class="layui-btn" lay-submit lay-filter="formDemo" id="selectOrder">检索</button>--%>
            </div>
        </form>
        <script type="text/javascript" src="${basePath}js/jquery.min.js"></script>
        <script type="text/javascript">
            $(function () {
                $("#sendOrder").click(function () {
                    location.href=("/order/updateState");
                })
            })
        </script>
        <script>
            layui.use('form', function() {
                var form = layui.form;

                //监听提交
                form.on('submit(formDemo)', function(data) {
                    layer.msg(JSON.stringify(data.field));
                    // location.href=("/order/findAllFood")
                    return false;
                });
            });
        </script>
    </div>
<form action="${basePath}order/updateState" method="post">
    <table class="layui-table" id="customList">
        <thead>
        <tr>
            <th>订单号</th>
            <th>桌号</th>
            <th>服务员姓名</th>
            <th>开始时间</th>
            <th>食品列表</th>
            <th>总价</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${list}" var="order">
            <tr>
            <td>${order.orderId}</td>
            <td>${order.tableNum}</td>
            <td>${order.waiterDO.waiterName}</td>
            <td>${order.orderCreateTime}</td>
            <td>${order.orderDetile}</td>
            <td>${order.orderSum}</td>
            <td>
                <%--<a href="${basePath}order/updateState?id=${order.orderId}"><button class="layui-btn layui-btn-xs">发送订单信息</button></a>--%>
                    <a href="${basePath}order/updateState?id=${order.orderId}" style="color: blue">发送订单信息</a>
            </td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
</form>
    <!-- layUI 分页模块 -->
    <div id="pages"></div>
    <script>
        /*layui.use('laypage', function() {
            var laypage = layui.laypage;

            //总页数大于页码总数
            laypage.render({
                elem: 'pages'
                ,count: 100
                ,layout: ['count', 'prev', 'page', 'next', 'limit', 'skip']
                ,jump: function(obj){
//					      console.log(obj)
                }
            });
        });*/

        //修改规格
        function specificationsBut(){
            layui.use('layer', function() {
                var layer = layui.layer;

                //iframe层-父子操作
                layer.open({
                    type: 2,
                    area: ['70%', '60%'],
                    fixed: false, //不固定
                    maxmin: true,
                    content: 'weihu.html'
                });
            });

        }
        //通知
        function msg(_this){
            layui.use(['form','laydate'], function() {
                layer.confirm('是否将结算消息发送给管理员', {
                    btn: ['确定', '取消'] //按钮
                }, function() {
                    $(_this).parent().parent().remove();
                    layer.msg('发送成功', {
                        icon: 1
                    });
                }, function() {
                    layer.msg('取消发送', {
                        time: 2000 //20s后自动关闭
                    });
                });
            });
        }

    </script>
</div>
</body>

</html>
