<%--
  Created by IntelliJ IDEA.
  User: 胡中宝
  Date: 2020/7/29
  Time: 17:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1">
    <!-- Google Chrome Frame也可以让IE用上Chrome的引擎: -->
    <meta name="renderer" content="webkit">
    <!--国产浏览器高速模式-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 网站简介 -->
    <meta name="keywords" content="搜索关键字，以半角英文逗号隔开" />
    <title>点餐系统</title>

    <!-- 公共样式 开始 -->
    <link rel="shortcut icon" href="images/login_bg2.ico"/>
    <link rel="bookmark" href="images/login_bg2.ico"/>
    <link rel="stylesheet" type="text/css" href="css/base.css">
    <link rel="stylesheet" type="text/css" href="css/iconfont.css">
    <script type="text/javascript" src="framework/jquery-1.11.3.min.js" ></script>
    <link rel="stylesheet" type="text/css" href="layui/css/layui.css">
    <!--[if lt IE 9]>
    <script src="framework/html5shiv.min.js"></script>
    <script src="framework/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="layui/layui.js"></script>
    <!-- 滚动条插件 -->
    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css">
    <script src="framework/jquery-ui-1.10.4.min.js"></script>
    <script src="framework/jquery.mousewheel.min.js"></script>
    <script src="framework/jquery.mCustomScrollbar.min.js"></script>
    <script src="framework/cframe.js"></script><!-- 仅供所有子页面使用 -->
    <!-- 公共样式 结束 -->

    <link rel="stylesheet" type="text/css" href="css/frameStyle.css">
    <script type="text/javascript" src="framework/frame.js" ></script>

</head>

<body>
<!-- 左侧菜单 - 开始 -->
<div class="frameMenu" style="background-image:url(images/222.jpg) ;background-size: cover;">
    <div class="logo">
        <img src="images/login_bg2.jpg"/>
        <div class="logoText">
            <h1>点餐系统</h1>

        </div>
    </div>
    <div class="menu">
        <ul>

            <li>
                <a class="menuFA" href="javascript:void(0)"><i class="iconfont icon-yonghu left"></i>点餐管理<i class="iconfont icon-dajiantouyou right"></i></a>
                <dl>
                    <dt><a href="javascript:void(0)" onclick="menuCAClick('/order/findAllFood',this)">点餐</a></dt>
                    <dt><a href="javascript:void(0)" onclick="menuCAClick('/order/findAllOrder',this)">结算</a></dt>
                </dl>
            </li>


        </ul>
    </div>
</div>
<!-- 左侧菜单 - 结束 -->

<div class="main">
    <!-- 头部栏 - 开始 -->
    <div class="frameTop" style="background-image:url(images/222.jpg) ;background-size: cover;">
        <img class="jt" src="images/top_jt.png"/>
        <div class="topMenu" >
            <ul>
                <li style="background-color:rgb(9, 4, 78);"><a href="javascript:void(0)" onclick="menuCAClick('tgls/modify_password.html',this)"><i class="iconfont icon-yonghu1"></i>服务员001</a></li>
                <li style="background-color:rgb(9, 4, 78);"><a href="javascript:void(0)" onclick="menuCAClick('tgls/modify_password.html',this)"><i class="iconfont icon-xiugaimima"></i>修改密码</a></li>
                <li style="background-color:rgb(9, 4, 78);"><a href="01_login.html"><i class="iconfont icon-084tuichu"></i>注销</a></li>
            </ul>
        </div>
    </div>
    <!-- 头部栏 - 结束 -->

    <!-- 核心区域 - 开始 -->
    <div class="frameMain" >
        <div class="title" id="frameMainTitle" style="background-image:url(images/222.jpg) ;background-size: cover;">
            <span><i class="iconfont icon-xianshiqi"></i>后台首页</span>
        </div>
        <div class="con">
            <iframe id="mainIframe" src="${basePath }index.jsp" scrolling="no"></iframe>
        </div>
    </div>
    <!-- 核心区域 - 结束 -->
</div>
</body>

</html>
