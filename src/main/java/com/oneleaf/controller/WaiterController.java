package com.oneleaf.controller;


import com.oneleaf.entity.AllUserDO;
import com.oneleaf.entity.WaiterDO;
import com.oneleaf.service.AllUserService;
import com.oneleaf.service.WaiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@RequestMapping("/waiter")
public class WaiterController extends BaseController{

    @Autowired
    private WaiterService waiterService;

    @Autowired
    private AllUserService allUserService;

    /**
     * 进入13_employeeUpdate.jsp的方法
     * @return
     */
    @RequestMapping("/employeeUpdate")
    public String employeeUpdate(){
        return "13_employeeUpdate";
    }

    /**
     * 列出所有服务员人员的基本信息
     * @return
     */
    /*@RequestMapping("/employeelist")
    public String list(HttpSession session){
        List<WaiterDO> waiters = waiterService.listWaiter();
        session.setAttribute("waiters", waiters);
        System.out.println(waiters);
        return "05_employeelist";
    }*/

    @RequestMapping("/employeelist")
    public String list(HttpServletRequest request, WaiterDO waiterDO){
        init(request);//初始化起始位置
        if(waiterDO == null ){
            waiterDO = new WaiterDO();
        }
        //System.out.println("Controller = employeelist");
        //取出查询参数，放入request，作为页面上页码超链接的参数
        String kName = waiterDO.getWaiterName();
        request.setAttribute("kName", kName);

        waiterDO.setStart(getStart());//起始记录数
        waiterDO.setLength(PAGE_NUM);//每页显示的记录数
        List<WaiterDO> waiters = waiterService.listWaiterByCondition(waiterDO);//这一页的用户记录数据
        Long total = waiterService.getWaiterCount(waiterDO);//记录总数
        request.setAttribute("waiters", waiters);
        request.setAttribute("total", total);

        return "08_waiterlist";
    }

    /**
     * 进入13_employeeUpdate.jsp的方法
     * @param waiterDO
     * @param session
     * @return
     */
    @RequestMapping("/update")
    public String update(WaiterDO waiterDO, HttpSession session , HttpServletRequest request){
        //获取前端传来的waiterId
        String id = request.getParameter("id");
        Integer idd = Integer.parseInt(id);
        //创建WaiterDO对象，设置waiterId并查询这个waiterId的所有信息
        WaiterDO updateUser1 = new WaiterDO();
        //System.out.println("Controller = update " + idd);
        updateUser1.setWaiterId(idd);
        //创建新对象得到全部信息
        WaiterDO updateUser = waiterService.getUpdateWaiter(updateUser1);
        //System.out.println("Controller = update1 " + updateUser.toString());
        session.setAttribute("updateUser", updateUser);
        //得到与waiterDO对象相同userId的AllUserDO
        Integer userid = updateUser.getAllUserDO().getUserId();
        AllUserDO allUser1 = new AllUserDO();
        allUser1.setUserId(userid);
        AllUserDO allUser = allUserService.login(allUser1);
        session.setAttribute("allUser",allUser);
        //System.out.println("Controller = update2 " + allUser.toString());
        return "16_waiterUpdate";
    }

    /**
     * 更新用户基本信息的方法
     * @param waiterDO
     * @return
     */
    @RequestMapping("/doupdate")
    public String doUpdate(WaiterDO waiterDO,HttpSession session, HttpServletRequest request){
        //System.out.println("Controller = doupdate " + waiterDO.toString());
        waiterService.updateWaiter(waiterDO);

        //提取出AllUserDO对象
        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserId(Integer.parseInt(request.getParameter("allUserDO.userId")));
        allUserDO.setUserName(request.getParameter("allUserDO.userName"));
        allUserDO.setUserNumber(request.getParameter("allUserDO.userNumber"));
        allUserDO.setUserPassword(request.getParameter("allUserDO.userPassword"));
        //allUserDO.setUserImageUrl("222");
        allUserDO.setUserType("waiter");
        allUserService.updateAllUser(allUserDO);

        return "redirect:/waiter/employeelist";
    }

    /**
     * 删除用户
     * @param waiterDO
     * @return
     */
    @RequestMapping("/delete")
    public String doDelete(WaiterDO waiterDO,HttpSession session, HttpServletRequest request){
        //获取前端传来的waiterId
        String id = request.getParameter("id");
        Integer idd = Integer.parseInt(id);
        waiterDO.setWaiterId(idd);
        //System.out.println("Controller = delete " + waiterDO);

        //System.out.println("Controller = delete1 " + idd);
        WaiterDO deleteUser1 = new WaiterDO();
        deleteUser1.setWaiterId(idd);

        //创建新对象得到全部信息
        WaiterDO deleteUser = waiterService.getUpdateWaiter(deleteUser1);
        //System.out.println("Controller = delete2 " + deleteUser);
        Integer userid = deleteUser.getAllUserDO().getUserId();
        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserId(userid);
        //System.out.println("Controller = delete3 " + allUserDO);

        waiterService.deleteWaiter(waiterDO);
        allUserService.deleteAllUser(allUserDO);
        return "redirect:/waiter/employeelist";
    }

    
}
