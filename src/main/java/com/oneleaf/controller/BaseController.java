package com.oneleaf.controller;

import javax.servlet.http.HttpServletRequest;

/**
 * 所有Controller的父类，处理分页信息
 *
 */
public class BaseController {
	/**
	 * 起始记录数
	 */
	private int start;
	/**
	 * 记录总数
	 */
	private Long total;
	/**
	 * 每页显示的记录数
	 */
	public final static int PAGE_NUM = 5;
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public Long getTotal() {
		return total;
	}
	public void setTotal(Long total) {
		this.total = total;
	}
	public void init(HttpServletRequest request){
		String page_str = request.getParameter("pager.offset");//获得起始记录数
		if(page_str != null && !page_str.equals("")){
			start = Integer.parseInt(page_str);
		}else{
			start = 0;
		}
	}
}
