package com.oneleaf.controller;

import com.oneleaf.entity.OrderDO;
import com.oneleaf.service.AdminOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/adminOrder")
public class AdminOrderController {

    @Autowired
    private AdminOrderService adminOrderService;
    //查找所有未结算订单
    @RequestMapping("/findNotEndOrder")
    public ModelAndView findNotEndOrder(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("11_order");
        modelAndView.addObject("list",adminOrderService.findNotEndOrder());
        return modelAndView;
    }


    //修改订单结账状态

    @RequestMapping("/updateOrderState")
    public String updateOrderState(Integer id){
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(date);
        OrderDO orderDO = new OrderDO();
        orderDO.setOrderId(id);
        orderDO.setOrderEndTime(time);
        adminOrderService.updateOrderState(orderDO);
        return "redirect:/adminOrder/findNotEndOrder";
    }

    /*//查找所有结账完成的订单
    @RequestMapping("/findEndOrder")
    public String findEndOrder(HttpServletRequest request,OrderDO orderDO){
        init(request);
        if(orderDO == null){
            orderDO = new OrderDO();
        }
        Integer tableNum = orderDO.getTableNum();
        request.setAttribute("tableNum",tableNum);
        orderDO.setStart(getStart());
        orderDO.setLength(3);

        List<OrderDO> orderDOS = adminOrderService.findEndOrder(orderDO);
        Long total = adminOrderService.getOrderCompleteCount(orderDO);
        request.setAttribute("list1",orderDOS);
        request.setAttribute("total",total);
//
//        ModelAndView modelAndView = new ModelAndView();
//        modelAndView.setViewName("11_Endorder");
//        modelAndView.addObject("list1",adminOrderService.findEndOrder());
        return "11_Endorder";
    }*/

    //查找所有结账完成的订单
    @RequestMapping("/findEndOrder")
    public ModelAndView findEndOrder(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("11_Endorder");
        modelAndView.addObject("list1",adminOrderService.findEndOrder());
        return modelAndView;
    }

}
