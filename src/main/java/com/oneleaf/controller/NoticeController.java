package com.oneleaf.controller;

import com.oneleaf.entity.BaseDO;
import com.oneleaf.entity.NoticeDO;
import com.oneleaf.service.AllUserService;
import com.oneleaf.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/notice")
public class NoticeController extends BaseController {

    @Autowired
    private NoticeService noticeService;

    /*
    * 查询所有公告
    * */
    @RequestMapping("/getNotice00")
    public String getNotice(HttpSession session){
        //System.out.println("成功抓取noticelist请求");
        List<NoticeDO> result=noticeService.getNotice();
        session.setAttribute("noticeList",result);
        return "noticelist";
    }

    /*
     * 查询所有公告
     * */
    @RequestMapping("/getNotice")
    public String getNotice(HttpServletRequest request, NoticeDO noticeDO){
        init(request);
        if (noticeDO == null){
            noticeDO = new NoticeDO();
        }
        noticeDO.setStart(getStart());
        noticeDO.setLength(5);
        List<NoticeDO> result=noticeService.getNoticePage(noticeDO);
        Long total = noticeService.getNoticeCount(noticeDO);
        request.setAttribute("noticeList",result);
        request.setAttribute("total",total);
        return "noticelist";
    }
    /*
    * 添加公告
    * */
    @RequestMapping("/addNotice")
    public String addNotice(NoticeDO noticeDO,HttpSession session){
        noticeService.addNotice(noticeDO);
        List<NoticeDO> result=noticeService.getNotice();
        session.setAttribute("noticeList",result);
        return "addnotice";
    }
    /*
     * 跳转到更新公告页面
     * */
    @RequestMapping("/updateNotice")
    public String updateNotice(NoticeDO noticeDO,HttpSession session,Integer noticeid){
        System.out.println("选的是这个公告,ID2:"+noticeid);
        NoticeDO updatenoticeDO = new NoticeDO();
        updatenoticeDO=noticeService.getNoticeById(noticeid);
        session.setAttribute("NoticeDO",updatenoticeDO);
        //noticeService.updateNotice(noticeDO);
        System.out.println("选的是这个公告,ID2:"+updatenoticeDO);
        return "updatenotice";
    }
    /*
     * 删除公告
     * */
    @RequestMapping("/deleteNotice")
    public String deleteNotice(Integer noticeid,HttpSession session){
        System.out.println("选的是这个公告,ID1:"+noticeid);
        noticeService.deleteNoticeById(noticeid);
        List<NoticeDO> result=noticeService.getNotice();
        session.setAttribute("noticeList",result);
        return "noticelist";
    }
    /*
     * 更新公告
     * */
    @RequestMapping("/updateNotice2")
    public String updateNotice2(NoticeDO noticeDO,HttpSession session,Integer noticeid){
        noticeDO.setNoticeId(noticeid);
        System.out.println(noticeDO);
        noticeService.updateNotice2(noticeDO);
        List<NoticeDO> result=noticeService.getNotice();
        session.setAttribute("noticeList",result);
        return "noticelist";
    }

    /*
     * 服务员、厨师查看公告
     * */
    @RequestMapping("/checkNotice")
    public String checkNotice(HttpSession session){
        List<NoticeDO> result=noticeService.getNotice();
        session.setAttribute("noticeList",result);
        return "checknotice";
    }
}
