package com.oneleaf.controller;


import com.oneleaf.entity.AllUserDO;
import com.oneleaf.entity.KitchenDO;
import com.oneleaf.service.AllUserService;
import com.oneleaf.service.KitchenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;


@Controller
@RequestMapping("/kitchen")
public class KitchenController extends BaseController{

    @Autowired
    private KitchenService kitchenService;

    @Autowired
    private AllUserService allUserService;

    /**
     * 进入13_employeeUpdate.jsp的方法
     * @return
     */
    @RequestMapping("/employeeUpdate")
    public String employeeUpdate(){
        return "13_employeeUpdate";
    }



    /**
     * 列出所有后厨人员的基本信息
     * @return
     */
    /*@RequestMapping("/employeelist")
    public String list(HttpSession session){
        List<KitchenDO> kitchens = kitchenService.listKitchen();
        session.setAttribute("kitchens", kitchens);
        System.out.println(kitchens);
        return "05_employeelist";
    }*/

    @RequestMapping("/employeelist")
    public String list(HttpServletRequest request, KitchenDO kitchenDO){
        init(request);//初始化起始位置
        if(kitchenDO == null ){
            kitchenDO = new KitchenDO();
        }
        //System.out.println("Controller = employeelist");
        //取出查询参数，放入request，作为页面上页码超链接的参数
        String kName = kitchenDO.getKitchenName();
        request.setAttribute("kName", kName);

        kitchenDO.setStart(getStart());//起始记录数
        kitchenDO.setLength(PAGE_NUM);//每页显示的记录数
        List<KitchenDO> kitchens = kitchenService.listKitchenByCondition(kitchenDO);//这一页的用户记录数据
        Long total = kitchenService.getKitchenCount(kitchenDO);//记录总数
        request.setAttribute("kitchens", kitchens);
        request.setAttribute("total", total);

        return "05_employeelist";
    }

    /**
     * 进入13_employeeUpdate.jsp的方法
     * @param kitchenDO
     * @param session
     * @return
     */
    @RequestMapping("/update")
    public String update(KitchenDO kitchenDO, HttpSession session , HttpServletRequest request){
        //获取前端传来的kitchenId
        String id = request.getParameter("id");
        Integer idd = Integer.parseInt(id);
        //创建KitchenDO对象，设置kitchenId并查询这个kitchenId的所有信息
        KitchenDO updateUser1 = new KitchenDO();
        //System.out.println("Controller = update " + idd);
        updateUser1.setKitchenId(idd);
        //创建新对象得到全部信息
        KitchenDO updateUser = kitchenService.getUpdateKitchen(updateUser1);
        //System.out.println("Controller = update1 " + updateUser.toString());
        session.setAttribute("updateUser", updateUser);
        //得到与kitchenDO对象相同userId的AllUserDO
        Integer userid = updateUser.getAllUserDO().getUserId();
        AllUserDO allUser1 = new AllUserDO();
        allUser1.setUserId(userid);
        AllUserDO allUser = allUserService.login(allUser1);
        session.setAttribute("allUser",allUser);
        //System.out.println("Controller = update2 " + allUser.toString());
        return "13_employeeUpdate";
    }

    /**
     * 更新用户基本信息的方法
     * @param kitchenDO
     * @return
     */
    @RequestMapping("/doupdate")
    public String doUpdate(KitchenDO kitchenDO,HttpSession session, HttpServletRequest request){
        //System.out.println("Controller = doupdate " + kitchenDO.toString());
        kitchenService.updateKitchen(kitchenDO);

        //提取出AllUserDO对象
        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserId(Integer.parseInt(request.getParameter("allUserDO.userId")));
        allUserDO.setUserName(request.getParameter("allUserDO.userName"));
        allUserDO.setUserNumber(request.getParameter("allUserDO.userNumber"));
        allUserDO.setUserPassword(request.getParameter("allUserDO.userPassword"));
        //allUserDO.setUserImageUrl("222");
        allUserDO.setUserType("kitchen");
        allUserService.updateAllUser(allUserDO);

        return "redirect:/kitchen/employeelist";
    }

    /**
     * 删除用户
     * @param kitchenDO
     * @return
     */
    @RequestMapping("/delete")
    public String doDelete(KitchenDO kitchenDO,HttpSession session, HttpServletRequest request){
        //获取前端传来的kitchenId
        String id = request.getParameter("id");
        Integer idd = Integer.parseInt(id);
        kitchenDO.setKitchenId(idd);
        //System.out.println("Controller = delete " + kitchenDO);

        //System.out.println("Controller = delete1 " + idd);
        KitchenDO deleteUser1 = new KitchenDO();
        deleteUser1.setKitchenId(idd);

        //创建新对象得到全部信息
        KitchenDO deleteUser = kitchenService.getUpdateKitchen(deleteUser1);
        //System.out.println("Controller = delete2 " + deleteUser);
        Integer userid = deleteUser.getAllUserDO().getUserId();
        AllUserDO allUserDO = new AllUserDO();
        allUserDO.setUserId(userid);
        //System.out.println("Controller = delete3 " + allUserDO);

        kitchenService.deleteKitchen(kitchenDO);
        allUserService.deleteAllUser(allUserDO);
        return "redirect:/kitchen/employeelist";
    }

    
}
