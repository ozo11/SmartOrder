package com.oneleaf.controller;

import com.oneleaf.entity.BaseDO;
import com.oneleaf.entity.DishesDO;
import com.oneleaf.entity.DishesImageDO;
import com.oneleaf.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/food")
public class FoodController extends BaseDO {

    @Autowired
    private FoodService foodService;

    /**
     * 进入addfood.jsp的方法
     * @return
     */
    @RequestMapping("/addfood")
    public String login(){
        return "addfood";
    }

    //查询出所有菜品种类
    @RequestMapping("/foodType")
    public String findFoodType(HttpServletRequest request){
        request.setAttribute("foodtype",foodService.findAllDishesType());
        return "addfood";
    }
    //查询所有菜品信息
    @GetMapping("/findAllFood")
    public ModelAndView findAllFood(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("06_foodlist");
        modelAndView.addObject("list",foodService.findAllDishes());
//        modelAndView.addObject("picture",foodService.findAllPicture());
        return modelAndView;
    }

    @PostMapping("/addFood")
    public String AddFood(DishesDO dishesDO, HttpServletResponse response, HttpServletRequest request,String dishesName){
        foodService.saveFood(dishesDO);
        /*return "redirect:/food/findAllFood";*/
        return "addfood";
    }

    //查找菜品
    @GetMapping("/findFoodById")
    public ModelAndView findFoodById(HttpServletRequest request){
        String idstr=request.getParameter("id");
        Integer dishesId = Integer.parseInt(idstr);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("14_goodsUpdate");
        modelAndView.addObject("dishes",foodService.findFoodById(dishesId));
        DishesDO dishesDO = foodService.findFoodById(dishesId);
        System.out.println(dishesDO);
        return modelAndView;
    }

    //修改商品信息
    @PostMapping("/updateFood")
    public String updateFood(DishesDO dishesDO){
        System.out.println(dishesDO);
        foodService.updateFood(dishesDO);
        return "redirect:/food/findAllFood";
    }
    //跳转到文件上传界面

    @GetMapping("/findFile")
    public String findFile(Integer id,HttpServletRequest request){
        DishesDO dishesDO = new DishesDO();
        dishesDO.setDishesId(id);
        request.setAttribute("dishesDo",dishesDO);
        request.setAttribute("list",foodService.findAllPicture(id));
        return "admin_upload";
    }

    //图片文件上传
    @PostMapping("/uploads")
    public ModelAndView uploads(@RequestParam("file") MultipartFile[] files, HttpServletRequest request,Integer id) throws IOException {
        System.out.println(id);
        ModelAndView mv = new ModelAndView();
        ServletContext servletContext = request.getSession().getServletContext();
        String realPath = servletContext.getRealPath("/uploadMultipal/");
        System.out.println("文件存放的位置： " + realPath);
        int i=0;
        File file1 = new File(realPath);
        if (!file1.exists()){
            file1.mkdirs();
        }
        for(MultipartFile file : files){
            System.out.println("现在存储 " + file.getOriginalFilename());
            String name = file.getOriginalFilename();
            String filename = UUID.randomUUID().toString().replace("-","").toUpperCase() + "_" + file.getOriginalFilename();
            file.transferTo(new File(realPath + name));
            DishesDO dishesDO = new DishesDO();
            dishesDO.setDishesId(id);
            DishesImageDO dishesImageDO = new DishesImageDO();
            dishesImageDO.setDishesDO(dishesDO);
            dishesImageDO.setDishesImageName(name);
            dishesImageDO.setDishesImageUrl(realPath+name);
            foodService.saveFile(dishesImageDO);
//            imageService.saveImage(filename);
        }
        request.setAttribute("list",foodService.findAllPicture(id));
        mv.setViewName("admin_upload");
        return mv;
    }

    //查看菜品的图片

    @RequestMapping("/findPicture")
    public String findPicture(Integer id,HttpServletRequest request){

        int i=0;
        return "picture";
    }

    //测试方法
    @GetMapping("/test")
    public String test(HttpServletRequest request){
        List<DishesImageDO> list = foodService.testPicture();
        request.setAttribute("list",list);
        return "testfood";
    }

    //删除菜品信息

    @RequestMapping("/deleteFood")
    public String deleteFood(Integer id){
        System.out.println(id);
        foodService.deleteFoodById(id);
        return "redirect:/food/findAllFood";
    }

    //查看菜品所有图片

    @RequestMapping("/findAllPicture")
    public ModelAndView FindAllPicture(Integer id,HttpServletRequest request){
        ServletContext servletContext = request.getServletContext();
        String path = servletContext.getRealPath("/uploadMultipal");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("pictureList");
        List<DishesImageDO> dishesImageDOS = foodService.findAllPicture(id);
        modelAndView.addObject("list",dishesImageDOS);
        request.setAttribute("path",path);
        int i = 0;
        return modelAndView;
    }

    //查找菜品信息

    @RequestMapping("/selectFood")
    public ModelAndView selectFood(String sourch){
        System.out.println(sourch);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("sourchfoodlist");
        modelAndView.addObject("list",foodService.findDishesDO(sourch));
        return modelAndView;
    }

}
