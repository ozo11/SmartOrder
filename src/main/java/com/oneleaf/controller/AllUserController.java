package com.oneleaf.controller;

import com.oneleaf.entity.AllUserDO;
import com.oneleaf.entity.KitchenDO;
import com.oneleaf.service.AllUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/user")
public class AllUserController {

    @Autowired
    private AllUserService allUserService;

    private String descpath;

    /**
     * 进入login.jsp的方法
     * @return
     */
    @RequestMapping("/login")
    public String login(){
        return "login";
    }
    /**
     * 做登录的方法
     * 控制方法
     * @return 视图名称  src/main/webapp下
     */
    @RequestMapping("/dologin")
    public String doLogin(AllUserDO allUserDO, HttpSession session, HttpServletRequest request){
        //取出所选用户类型
        String radioButton = request.getParameter("alluser");
        allUserDO.setUserType(radioButton);
        //System.out.println("1usertype = " + radioButton);
        AllUserDO loginUser = allUserService.login(allUserDO);
        System.out.println("loginUser = "+loginUser);
        if(loginUser != null){
            System.out.println("登录成功");
            session.setAttribute("loginUser", loginUser);
            return radioButton + "_index";
        }
        System.out.println("登录失败，请重新输入");
        return "login";
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public int uploadHead(@RequestParam("file") MultipartFile file, HttpSession session) {
        try {
            if (file != null) {
                //获取文件名
                String fileName = file.getOriginalFilename();
                //截取扩展名
                String extName = fileName.substring(fileName.lastIndexOf("."));
                List list = new ArrayList();
                list.add(".jpg");
                list.add(".png");
                list.add(".jpeg");
                list.add(".gif");
                if(list.contains(extName.toLowerCase())){

                    //01获取服务器项目部署的路径，并在根目录下创建 myphoto 目录
                    String realPath = session.getServletContext().getRealPath("userImage");
                    //02也可以直接定义文件路径
                    //String realPath = "D:\\space\\SSMBase\\src\\main\\webapp\\img";
                    System.out.println(realPath);
                    String photoFileName = new Date().getTime()+extName;
                    String descPath = realPath + "\\" + photoFileName;
                    descpath = realPath + "\\" + photoFileName;
                    session.setAttribute("descPath",descPath);
                    System.out.println(descPath);
                    file.transferTo(new File(realPath,photoFileName));
                    return 1;//成功
                }else {
                    return -1;//文件类型不正确
                }
            }else {
                return 0;//上传文件为空
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;//上传异常
        }
    }

    /**
     * 跳转到添加员工用户的页面
     * @return
     */
    @RequestMapping("/adduser")
    public String add(){
        return "04_addempolyee";
    }

    @RequestMapping("/doadduser")
    public String addAllUser(AllUserDO allUserDO, HttpServletRequest request){
        String typeselect = request.getParameter("userType");
        String descPath = request.getParameter("descPath");
        System.out.println("1usertype = " + typeselect);
        System.out.println("descPath = " + descPath);

        /*if(descPath != null){*/
            allUserDO.setUserType(typeselect);
            allUserDO.setUserImageUrl(descpath);
            //allUserDO.setUserImageUrl(descPath);
            allUserService.saveAllUser(allUserDO);
        /*}*/
        return "04_addempolyee";
    }

    /**
     * 跳转到更新员工用户的页面
     * @return
     */
    @RequestMapping("/updateuser")
    public String update(){
        return "13_employeeUpdate";
    }

    @RequestMapping("/doupdate")
    public String doUpdate(KitchenDO kitchenDO, HttpSession session, HttpServletRequest request){
        System.out.println("Controller : user/doupdate" + kitchenDO);

        //allUserService.updateAllUser(allUserDO);


        return "13_employeeUpdate";//刷新首页上的用户登录信息
    }

}
