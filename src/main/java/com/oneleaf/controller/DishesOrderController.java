package com.oneleaf.controller;

import com.oneleaf.dao.DishesOrderDAO;
import com.oneleaf.entity.DishesOrderDO;
import com.oneleaf.service.DishesOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/DishesOrder")
public class DishesOrderController {

    @Autowired
    private DishesOrderService dishesOrderService;
    @RequestMapping("/list")
    public String list(HttpServletRequest request,HttpSession session){
        List<DishesOrderDO> orderDOS = dishesOrderService.listDishesorder();
        session.setAttribute("orderDOS",orderDOS);
        return "07_readydish";
    }
    @RequestMapping("/cookedlist")
    public String cookedlist(HttpServletRequest request,HttpSession session){
        List<DishesOrderDO> orderDOS1 = dishesOrderService.listCookedOrder();
        session.setAttribute("orderDOS1",orderDOS1);
        return "08_cookdish";
    }
    @RequestMapping("/startorder")
    public String startorder(DishesOrderDO dishesOrderDO,HttpSession session){
        dishesOrderService.startDishesOrder(dishesOrderDO);
        return "redirect:/DishesOrder/list";
    }
    @RequestMapping("/endorder")
    public String endorder(DishesOrderDO dishesOrderDO){
        dishesOrderService.endDishesOrder(dishesOrderDO);
        return "redirect:/DishesOrder/list";
    }
}
