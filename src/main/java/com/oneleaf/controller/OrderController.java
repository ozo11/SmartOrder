package com.oneleaf.controller;

import com.oneleaf.entity.DishesDO;
import com.oneleaf.entity.DishesOrderDO;
import com.oneleaf.entity.OrderDO;
import com.oneleaf.entity.WaiterDO;
import com.oneleaf.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.xml.crypto.Data;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/order")
public class OrderController extends BaseController{

    @Autowired
    private OrderService orderService;
    //查找所有菜品信息
    @RequestMapping("/findAllFood")
    public String findAllFood(HttpServletRequest request){
        init(request);
        DishesDO dishesDO = new DishesDO();
        dishesDO.setStart(getStart());
        dishesDO.setLength(PAGE_NUM);
        List<DishesDO> list = orderService.findAllDishes(dishesDO);
        Long total = orderService.getDishesCount(dishesDO);
        request.setAttribute("list",list);
        request.setAttribute("total",total);
        //System.out.println(total);
        return "shoppingcar";
    }


    //接收点菜所有信息
    @RequestMapping("/findOrder")
    public String findOrder(Integer summer,Integer[] array,String[] arrayId,String tableNumber,HttpServletRequest request){
        //array数组为菜品数量，arrayId为菜品编号
        Integer table_Num = Integer.parseInt(tableNumber);
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(date);
        WaiterDO waiterDO = new WaiterDO();
        //waiterDO.setWaiterId(1);
        String u = request.getParameter("waiterId");
        Integer waiterid = Integer.parseInt(u);
        //System.out.println(u);
        waiterDO.setWaiterId(waiterid);
        DishesDO dishesDO = new DishesDO();
        DishesOrderDO dishesOrderDO = new DishesOrderDO();
        OrderDO orderDO = new OrderDO();

        for(int i=0;i<array.length;i++){
            int a = array[i];
            Integer b = Integer.parseInt(arrayId[i]);
            dishesDO.setDishesId(b);
            dishesOrderDO.setDishesDO(dishesDO);
            dishesOrderDO.setDishesAmount(a);
            dishesOrderDO.setDishesStartTime(time);
            dishesOrderDO.setWaiterDO(waiterDO);
            dishesOrderDO.setDishesStatus("未烹饪");
            dishesOrderDO.setTableNum(table_Num);
            orderService.saveOrderDishes(dishesOrderDO);
        }
        Integer tableNum = dishesOrderDO.getTableNum();
        List<DishesOrderDO> list = orderService.findOrder(tableNum);
        System.out.println(list);
        String orderDefine = "";
        Integer waiterId;
        String dishesNum = "";
        String allOrderDefind="";
        String test="";
        for(DishesOrderDO dishesOrder : list){
            orderDefine +=  dishesOrder.getDishesDO().getDishesName()+",";
            dishesNum += dishesOrder.getDishesAmount().toString()+",";
            allOrderDefind += dishesOrder.getDishesDO().getDishesName()+":"+dishesOrder.getDishesAmount().toString()+"\n";
            System.out.println(dishesOrder);
        }
        System.out.println(allOrderDefind);
        BigDecimal bigDecimal = new BigDecimal(summer.toString());
        orderDO.setOrderSum(bigDecimal);
        orderDO.setOrderCreateTime(time);
        orderDO.setOrderDetile(allOrderDefind);
        orderDO.setTableNum(tableNum);
        orderDO.setOrderStatus(1);
        orderDO.setWaiterDO(waiterDO);
        orderService.saveOrder(orderDO);
        return "redirect:/order/findAllFood";
    }
    //结算订单
    @RequestMapping("/findAllOrder")
    public ModelAndView findAllOrder(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("jiesuan");
        modelAndView.addObject("list",orderService.findOffOrder());
        return modelAndView;
    }
    //修改结算状态order/updateState
    @RequestMapping("/updateState")
    public String updateState(Integer id){
        orderService.updateState(id);
        return "redirect:/order/findAllOrder";
    }
    //查找点餐信息
    @RequestMapping("/selectOrder")
    public String selectOrder(Integer id, HttpServletRequest request){
        List<OrderDO> list = orderService.findOrderById(id);
        request.setAttribute("list",list);
        return "jiesuan";
    }
}
