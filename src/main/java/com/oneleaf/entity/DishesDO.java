package com.oneleaf.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class DishesDO extends BaseDO{
    private Integer dishesId;
    private DishesTypeDO dishesTypeDO;
    private String dishesName;
    private String dishesIntroduction;
    private String dishesDetail;
    private Integer dishesRecommend;
    private BigDecimal dishesPrice;
    private Integer dishesInventoryStatus;

    public DishesDO(Integer dishesId, DishesTypeDO dishesTypeDO, String dishesName, String dishesIntroduction, String dishesDetail, Integer dishesRecommend, BigDecimal dishesPrice, Integer dishesInventoryStatus) {
        this.dishesId = dishesId;
        this.dishesTypeDO = dishesTypeDO;
        this.dishesName = dishesName;
        this.dishesIntroduction = dishesIntroduction;
        this.dishesDetail = dishesDetail;
        this.dishesRecommend = dishesRecommend;
        this.dishesPrice = dishesPrice;
        this.dishesInventoryStatus = dishesInventoryStatus;
    }

    public DishesDO() {
    }

    public Integer getDishesId() {
        return dishesId;
    }

    public void setDishesId(Integer dishesId) {
        this.dishesId = dishesId;
    }

    public DishesTypeDO getDishesTypeDO() {
        return dishesTypeDO;
    }

    public void setDishesTypeDO(DishesTypeDO dishesTypeDO) {
        this.dishesTypeDO = dishesTypeDO;
    }

    public String getDishesName() {
        return dishesName;
    }

    public void setDishesName(String dishesName) {
        this.dishesName = dishesName;
    }

    public String getDishesIntroduction() {
        return dishesIntroduction;
    }

    public void setDishesIntroduction(String dishesIntroduction) {
        this.dishesIntroduction = dishesIntroduction;
    }

    public String getDishesDetail() {
        return dishesDetail;
    }

    public void setDishesDetail(String dishesDetail) {
        this.dishesDetail = dishesDetail;
    }

    public Integer getDishesRecommend() {
        return dishesRecommend;
    }

    public void setDishesRecommend(Integer dishesRecommend) {
        this.dishesRecommend = dishesRecommend;
    }

    public BigDecimal getDishesPrice() {
        return dishesPrice;
    }

    public void setDishesPrice(BigDecimal dishesPrice) {
        this.dishesPrice = dishesPrice;
    }

    public Integer getDishesInventoryStatus() {
        return dishesInventoryStatus;
    }

    public void setDishesInventoryStatus(Integer dishesInventoryStatus) {
        this.dishesInventoryStatus = dishesInventoryStatus;
    }

    @Override
    public String toString() {
        return "DishesDO{" +
                "dishesId=" + dishesId +
                ", dishesTypeDO=" + dishesTypeDO +
                ", dishesName='" + dishesName + '\'' +
                ", dishesIntroduction='" + dishesIntroduction + '\'' +
                ", dishesDetail='" + dishesDetail + '\'' +
                ", dishesRecommend=" + dishesRecommend +
                ", dishesPrice=" + dishesPrice +
                ", dishesInventoryStatus=" + dishesInventoryStatus +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishesDO)) return false;
        DishesDO dishesDO = (DishesDO) o;
        return Objects.equals(dishesId, dishesDO.dishesId) &&
                Objects.equals(dishesTypeDO, dishesDO.dishesTypeDO) &&
                Objects.equals(dishesName, dishesDO.dishesName) &&
                Objects.equals(dishesIntroduction, dishesDO.dishesIntroduction) &&
                Objects.equals(dishesDetail, dishesDO.dishesDetail) &&
                Objects.equals(dishesRecommend, dishesDO.dishesRecommend) &&
                Objects.equals(dishesPrice, dishesDO.dishesPrice) &&
                Objects.equals(dishesInventoryStatus, dishesDO.dishesInventoryStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishesId, dishesTypeDO, dishesName, dishesIntroduction, dishesDetail, dishesRecommend, dishesPrice, dishesInventoryStatus);
    }
}
