package com.oneleaf.entity;

import java.util.Objects;

public class AllUserDO {
    private Integer userId;
    private String userName;
    private String userNumber;
    private String userPassword;
    private String userImageUrl;
    private String userType;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public AllUserDO() {
    }

    public AllUserDO(Integer userId, String userName, String userNumber, String userPassword, String userImageUrl, String userType) {
        this.userId = userId;
        this.userName = userName;
        this.userNumber = userNumber;
        this.userPassword = userPassword;
        this.userImageUrl = userImageUrl;
        this.userType = userType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AllUserDO)) return false;
        AllUserDO allUserDO = (AllUserDO) o;
        return Objects.equals(userId, allUserDO.userId) &&
                Objects.equals(userName, allUserDO.userName) &&
                Objects.equals(userNumber, allUserDO.userNumber) &&
                Objects.equals(userPassword, allUserDO.userPassword) &&
                Objects.equals(userImageUrl, allUserDO.userImageUrl) &&
                Objects.equals(userType, allUserDO.userType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userName, userNumber, userPassword, userImageUrl, userType);
    }

    @Override
    public String toString() {
        return "AllUserDO{" +
                "userId=" + userId +
                ", userName='" + userName + '\'' +
                ", userNumber='" + userNumber + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userImageUrl='" + userImageUrl + '\'' +
                ", userType='" + userType + '\'' +
                '}';
    }



}

