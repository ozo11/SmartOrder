package com.oneleaf.entity;

/**
 * 所有DO的父类，包含分页信息
 *
 */
public class BaseDO {
	/**
	 * 起始的记录数
	 */
	private int start;
	/**
	 * 记录的长度
	 */
	private int length;
	public int getStart() {
		return start;
	}
	public void setStart(int start) {
		this.start = start;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
}
