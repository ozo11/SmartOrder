package com.oneleaf.entity;

import java.math.BigDecimal;
import java.util.Objects;

public class KitchenDO extends BaseDO{
    private Integer kitchenId;
    private AllUserDO allUserDO;
    private String kitchenName;
    private Integer kitchenSex;
    private Integer kitchenAge;
    private String kitchenEntryTime;
    private String kitchenPhonenum;

    public KitchenDO(Integer kitchenId, AllUserDO allUserDO, String kitchenName, Integer kitchenSex, Integer kitchenAge, String kitchenEntryTime, String kitchenPhonenum) {
        this.kitchenId = kitchenId;
        this.allUserDO = allUserDO;
        this.kitchenName = kitchenName;
        this.kitchenSex = kitchenSex;
        this.kitchenAge = kitchenAge;
        this.kitchenEntryTime = kitchenEntryTime;
        this.kitchenPhonenum = kitchenPhonenum;
    }

    public KitchenDO() {
    }

    public Integer getKitchenId() {
        return kitchenId;
    }

    public void setKitchenId(Integer kitchenId) {
        this.kitchenId = kitchenId;
    }

    public AllUserDO getAllUserDO() {
        return allUserDO;
    }

    public void setAllUserDO(AllUserDO allUserDO) {
        this.allUserDO = allUserDO;
    }

    public String getKitchenName() {
        return kitchenName;
    }

    public void setKitchenName(String kitchenName) {
        this.kitchenName = kitchenName;
    }

    public Integer getKitchenSex() {
        return kitchenSex;
    }

    public void setKitchenSex(Integer kitchenSex) {
        this.kitchenSex = kitchenSex;
    }

    public Integer getKitchenAge() {
        return kitchenAge;
    }

    public void setKitchenAge(Integer kitchenAge) {
        this.kitchenAge = kitchenAge;
    }

    public String getKitchenEntryTime() {
        return kitchenEntryTime;
    }

    public void setKitchenEntryTime(String kitchenEntryTime) {
        this.kitchenEntryTime = kitchenEntryTime;
    }

    public String getKitchenPhonenum() {
        return kitchenPhonenum;
    }

    public void setKitchenPhonenum(String kitchenPhonenum) {
        this.kitchenPhonenum = kitchenPhonenum;
    }

    @Override
    public String toString() {
        return "KitchenDO{" +
                "kitchenId=" + kitchenId +
                ", allUserDO=" + allUserDO +
                ", kitchenName='" + kitchenName + '\'' +
                ", kitchenSex=" + kitchenSex +
                ", kitchenAge=" + kitchenAge +
                ", kitchenEntryTime=" + kitchenEntryTime +
                ", kitchenPhonenum='" + kitchenPhonenum + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof KitchenDO)) return false;
        KitchenDO kitchenDO = (KitchenDO) o;
        return Objects.equals(kitchenId, kitchenDO.kitchenId) &&
                Objects.equals(allUserDO, kitchenDO.allUserDO) &&
                Objects.equals(kitchenName, kitchenDO.kitchenName) &&
                Objects.equals(kitchenSex, kitchenDO.kitchenSex) &&
                Objects.equals(kitchenAge, kitchenDO.kitchenAge) &&
                Objects.equals(kitchenEntryTime, kitchenDO.kitchenEntryTime) &&
                Objects.equals(kitchenPhonenum, kitchenDO.kitchenPhonenum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(kitchenId, allUserDO, kitchenName, kitchenSex, kitchenAge, kitchenEntryTime, kitchenPhonenum);
    }
}
