package com.oneleaf.entity;

import java.util.Objects;

public class DishesImageDO {
    private Integer dishesImageId;
    private DishesDO dishesDO;
    private String dishesImageName;
    private String dishesImageUrl;

    public DishesImageDO(Integer dishesImageId, DishesDO dishesDO, String dishesImageName, String dishesImageUrl) {
        this.dishesImageId = dishesImageId;
        this.dishesDO = dishesDO;
        this.dishesImageName = dishesImageName;
        this.dishesImageUrl = dishesImageUrl;
    }

    public DishesImageDO() {
    }

    public Integer getDishesImageId() {
        return dishesImageId;
    }

    public void setDishesImageId(Integer dishesImageId) {
        this.dishesImageId = dishesImageId;
    }

    public DishesDO getDishesDO() {
        return dishesDO;
    }

    public void setDishesDO(DishesDO dishesDO) {
        this.dishesDO = dishesDO;
    }

    public String getDishesImageName() {
        return dishesImageName;
    }

    public void setDishesImageName(String dishesImageName) {
        this.dishesImageName = dishesImageName;
    }

    public String getDishesImageUrl() {
        return dishesImageUrl;
    }

    public void setDishesImageUrl(String dishesImageUrl) {
        this.dishesImageUrl = dishesImageUrl;
    }

    @Override
    public String toString() {
        return "DishesImageDO{" +
                "dishesImageId=" + dishesImageId +
                ", dishesDO=" + dishesDO +
                ", dishesImageName='" + dishesImageName + '\'' +
                ", dishesImageUrl='" + dishesImageUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishesImageDO)) return false;
        DishesImageDO that = (DishesImageDO) o;
        return Objects.equals(dishesImageId, that.dishesImageId) &&
                Objects.equals(dishesDO, that.dishesDO) &&
                Objects.equals(dishesImageName, that.dishesImageName) &&
                Objects.equals(dishesImageUrl, that.dishesImageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishesImageId, dishesDO, dishesImageName, dishesImageUrl);
    }
}
