package com.oneleaf.entity;

import java.util.Objects;

public class DishesTypeDO {
    private Integer dishesTypeId;
    private String dishesTypeName;
    private String dishesTypeTaste;
    private String dishesTypeIntroduction;

    public DishesTypeDO(Integer dishesTypeId, String dishesTypeName, String dishesTypeTaste, String dishesTypeIntroduction) {
        this.dishesTypeId = dishesTypeId;
        this.dishesTypeName = dishesTypeName;
        this.dishesTypeTaste = dishesTypeTaste;
        this.dishesTypeIntroduction = dishesTypeIntroduction;
    }

    public DishesTypeDO() {
    }

    public Integer getDishesTypeId() {
        return dishesTypeId;
    }

    public void setDishesTypeId(Integer dishesTypeId) {
        this.dishesTypeId = dishesTypeId;
    }

    public String getDishesTypeName() {
        return dishesTypeName;
    }

    public void setDishesTypeName(String dishesTypeName) {
        this.dishesTypeName = dishesTypeName;
    }

    public String getDishesTypeTaste() {
        return dishesTypeTaste;
    }

    public void setDishesTypeTaste(String dishesTypeTaste) {
        this.dishesTypeTaste = dishesTypeTaste;
    }

    public String getDishesTypeIntroduction() {
        return dishesTypeIntroduction;
    }

    public void setDishesTypeIntroduction(String dishesTypeIntroduction) {
        this.dishesTypeIntroduction = dishesTypeIntroduction;
    }

    @Override
    public String toString() {
        return "DishesTypeDO{" +
                "dishesTypeId=" + dishesTypeId +
                ", dishesTypeName='" + dishesTypeName + '\'' +
                ", dishesTypeTaste='" + dishesTypeTaste + '\'' +
                ", dishesTypeIntroduction='" + dishesTypeIntroduction + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishesTypeDO)) return false;
        DishesTypeDO that = (DishesTypeDO) o;
        return Objects.equals(dishesTypeId, that.dishesTypeId) &&
                Objects.equals(dishesTypeName, that.dishesTypeName) &&
                Objects.equals(dishesTypeTaste, that.dishesTypeTaste) &&
                Objects.equals(dishesTypeIntroduction, that.dishesTypeIntroduction);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishesTypeId, dishesTypeName, dishesTypeTaste, dishesTypeIntroduction);
    }
}
