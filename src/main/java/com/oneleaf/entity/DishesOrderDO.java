package com.oneleaf.entity;

import java.util.Date;
import java.util.Objects;

public class DishesOrderDO {
    private Integer dishesOrderId;
    private DishesDO dishesDO;
    private Integer dishesAmount;
    private String dishesStartTime;
    private WaiterDO waiterDO;
    private String dishesStatus;
    private Integer tableNum;

    public DishesOrderDO(Integer dishesOrderId, DishesDO dishesDO, Integer dishesAmount, String dishesStartTime, WaiterDO waiterDO, String dishesStatus, Integer tableNum) {
        this.dishesOrderId = dishesOrderId;
        this.dishesDO = dishesDO;
        this.dishesAmount = dishesAmount;
        this.dishesStartTime = dishesStartTime;
        this.waiterDO = waiterDO;
        this.dishesStatus = dishesStatus;
        this.tableNum = tableNum;
    }

    public DishesOrderDO() {
    }

    public Integer getDishesOrderId() {
        return dishesOrderId;
    }

    public void setDishesOrderId(Integer dishesOrderId) {
        this.dishesOrderId = dishesOrderId;
    }

    public DishesDO getDishesDO() {
        return dishesDO;
    }

    public void setDishesDO(DishesDO dishesDO) {
        this.dishesDO = dishesDO;
    }

    public Integer getDishesAmount() {
        return dishesAmount;
    }

    public void setDishesAmount(Integer dishesAmount) {
        this.dishesAmount = dishesAmount;
    }

    public String getDishesStartTime() {
        return dishesStartTime;
    }

    public void setDishesStartTime(String dishesStartTime) {
        this.dishesStartTime = dishesStartTime;
    }

    public WaiterDO getWaiterDO() {
        return waiterDO;
    }

    public void setWaiterDO(WaiterDO waiterDO) {
        this.waiterDO = waiterDO;
    }

    public String getDishesStatus() {
        return dishesStatus;
    }

    public void setDishesStatus(String dishesStatus) {
        this.dishesStatus = dishesStatus;
    }

    public Integer getTableNum() {
        return tableNum;
    }

    public void setTableNum(Integer tableNum) {
        this.tableNum = tableNum;
    }

    @Override
    public String toString() {
        return "DishesOrderDO{" +
                "dishesOrderId=" + dishesOrderId +
                ", dishesDO=" + dishesDO +
                ", dishesAmount=" + dishesAmount +
                ", dishesStartTime='" + dishesStartTime + '\'' +
                ", waiterDO=" + waiterDO +
                ", dishesStatus='" + dishesStatus + '\'' +
                ", tableNum=" + tableNum +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishesOrderDO)) return false;
        DishesOrderDO that = (DishesOrderDO) o;
        return Objects.equals(dishesOrderId, that.dishesOrderId) &&
                Objects.equals(dishesDO, that.dishesDO) &&
                Objects.equals(dishesAmount, that.dishesAmount) &&
                Objects.equals(dishesStartTime, that.dishesStartTime) &&
                Objects.equals(waiterDO, that.waiterDO) &&
                Objects.equals(dishesStatus, that.dishesStatus) &&
                Objects.equals(tableNum, that.tableNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dishesOrderId, dishesDO, dishesAmount, dishesStartTime, waiterDO, dishesStatus, tableNum);
    }
}
