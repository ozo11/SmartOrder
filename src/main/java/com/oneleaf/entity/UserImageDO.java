package com.oneleaf.entity;

import java.util.Objects;

public class UserImageDO {
    private Integer userImageId;
    private String imageName;
    private String imageUrl;

    public UserImageDO(Integer userImageId, String imageName, String imageUrl) {
        this.userImageId = userImageId;
        this.imageName = imageName;
        this.imageUrl = imageUrl;
    }

    public UserImageDO() {
    }

    public Integer getUserImageId() {
        return userImageId;
    }

    public void setUserImageId(Integer userImageId) {
        this.userImageId = userImageId;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "UserImageDO{" +
                "userImageId=" + userImageId +
                ", imageName='" + imageName + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserImageDO)) return false;
        UserImageDO that = (UserImageDO) o;
        return Objects.equals(userImageId, that.userImageId) &&
                Objects.equals(imageName, that.imageName) &&
                Objects.equals(imageUrl, that.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userImageId, imageName, imageUrl);
    }
}
