package com.oneleaf.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class OrderDO extends BaseDO{
    private Integer orderId;
    private BigDecimal orderSum;
    private String orderCreateTime;
    private String orderEndTime;
    private String orderDetile;
    private Integer tableNum;
    private Integer orderStatus;
    private WaiterDO waiterDO;
    private DishesOrderDO dishesOrderDO;

    public OrderDO(Integer orderId, BigDecimal orderSum, String orderCreateTime, String orderEndTime, String orderDetile, Integer tableNum, Integer orderStatus, WaiterDO waiterDO, DishesOrderDO dishesOrderDO) {
        this.orderId = orderId;
        this.orderSum = orderSum;
        this.orderCreateTime = orderCreateTime;
        this.orderEndTime = orderEndTime;
        this.orderDetile = orderDetile;
        this.tableNum = tableNum;
        this.orderStatus = orderStatus;
        this.waiterDO = waiterDO;
        this.dishesOrderDO = dishesOrderDO;
    }

    public OrderDO() {
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public BigDecimal getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(BigDecimal orderSum) {
        this.orderSum = orderSum;
    }

    public String getOrderCreateTime() {
        return orderCreateTime;
    }

    public void setOrderCreateTime(String orderCreateTime) {
        this.orderCreateTime = orderCreateTime;
    }

    public String getOrderEndTime() {
        return orderEndTime;
    }

    public void setOrderEndTime(String orderEndTime) {
        this.orderEndTime = orderEndTime;
    }

    public String getOrderDetile() {
        return orderDetile;
    }

    public void setOrderDetile(String orderDetile) {
        this.orderDetile = orderDetile;
    }

    public Integer getTableNum() {
        return tableNum;
    }

    public void setTableNum(Integer tableNum) {
        this.tableNum = tableNum;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public WaiterDO getWaiterDO() {
        return waiterDO;
    }

    public void setWaiterDO(WaiterDO waiterDO) {
        this.waiterDO = waiterDO;
    }

    public DishesOrderDO getDishesOrderDO() {
        return dishesOrderDO;
    }

    public void setDishesOrderDO(DishesOrderDO dishesOrderDO) {
        this.dishesOrderDO = dishesOrderDO;
    }

    @Override
    public String toString() {
        return "OrderDO{" +
                "orderId=" + orderId +
                ", orderSum=" + orderSum +
                ", orderCreateTime='" + orderCreateTime + '\'' +
                ", orderEndTime='" + orderEndTime + '\'' +
                ", orderDetile='" + orderDetile + '\'' +
                ", tableNum=" + tableNum +
                ", orderStatus=" + orderStatus +
                ", waiterDO=" + waiterDO +
                ", dishesOrderDO=" + dishesOrderDO +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderDO)) return false;
        OrderDO orderDO = (OrderDO) o;
        return Objects.equals(orderId, orderDO.orderId) &&
                Objects.equals(orderSum, orderDO.orderSum) &&
                Objects.equals(orderCreateTime, orderDO.orderCreateTime) &&
                Objects.equals(orderEndTime, orderDO.orderEndTime) &&
                Objects.equals(orderDetile, orderDO.orderDetile) &&
                Objects.equals(tableNum, orderDO.tableNum) &&
                Objects.equals(orderStatus, orderDO.orderStatus) &&
                Objects.equals(waiterDO, orderDO.waiterDO) &&
                Objects.equals(dishesOrderDO, orderDO.dishesOrderDO);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderSum, orderCreateTime, orderEndTime, orderDetile, tableNum, orderStatus, waiterDO, dishesOrderDO);
    }
}


