package com.oneleaf.entity;

import java.util.Objects;

public class AdminDO {
    private Integer adminId;
    private AllUserDO allUserDO;
    private String adminName;
    private String adminPhonenum;

    public AdminDO(Integer adminId, AllUserDO allUserDO, String adminName, String adminPhonenum) {
        this.adminId = adminId;
        this.allUserDO = allUserDO;
        this.adminName = adminName;
        this.adminPhonenum = adminPhonenum;
    }

    public AdminDO() {
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public AllUserDO getAllUserDO() {
        return allUserDO;
    }

    public void setAllUserDO(AllUserDO allUserDO) {
        this.allUserDO = allUserDO;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminPhonenum() {
        return adminPhonenum;
    }

    public void setAdminPhonenum(String adminPhonenum) {
        this.adminPhonenum = adminPhonenum;
    }

    @Override
    public String toString() {
        return "AdminDO{" +
                "adminId=" + adminId +
                ", allUserDO=" + allUserDO +
                ", adminName='" + adminName + '\'' +
                ", adminPhonenum='" + adminPhonenum + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdminDO)) return false;
        AdminDO adminDO = (AdminDO) o;
        return Objects.equals(adminId, adminDO.adminId) &&
                Objects.equals(allUserDO, adminDO.allUserDO) &&
                Objects.equals(adminName, adminDO.adminName) &&
                Objects.equals(adminPhonenum, adminDO.adminPhonenum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(adminId, allUserDO, adminName, adminPhonenum);
    }
}
