package com.oneleaf.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

public class WaiterDO extends BaseDO{
    private Integer waiterId;
    private AllUserDO allUserDO;
    private String waiterName;
    private Integer waiterSex;
    private Integer waiterAge;
    private String waiterEntryTime;
    private String waiterPhonenum;

    public WaiterDO(Integer waiterId, AllUserDO allUserDO, String waiterName, Integer waiterSex, Integer waiterAge, String waiterEntryTime, String waiterPhonenum) {
        this.waiterId = waiterId;
        this.allUserDO = allUserDO;
        this.waiterName = waiterName;
        this.waiterSex = waiterSex;
        this.waiterAge = waiterAge;
        this.waiterEntryTime = waiterEntryTime;
        this.waiterPhonenum = waiterPhonenum;
    }

    public WaiterDO() {
    }

    public Integer getWaiterId() {
        return waiterId;
    }

    public void setWaiterId(Integer waiterId) {
        this.waiterId = waiterId;
    }

    public AllUserDO getAllUserDO() {
        return allUserDO;
    }

    public void setAllUserDO(AllUserDO allUserDO) {
        this.allUserDO = allUserDO;
    }

    public String getWaiterName() {
        return waiterName;
    }

    public void setWaiterName(String waiterName) {
        this.waiterName = waiterName;
    }

    public Integer getWaiterSex() {
        return waiterSex;
    }

    public void setWaiterSex(Integer waiterSex) {
        this.waiterSex = waiterSex;
    }

    public Integer getWaiterAge() {
        return waiterAge;
    }

    public void setWaiterAge(Integer waiterAge) {
        this.waiterAge = waiterAge;
    }

    public String getWaiterEntryTime() {
        return waiterEntryTime;
    }

    public void setWaiterEntryTime(String waiterEntryTime) {
        this.waiterEntryTime = waiterEntryTime;
    }

    public String getWaiterPhonenum() {
        return waiterPhonenum;
    }

    public void setWaiterPhonenum(String waiterPhonenum) {
        this.waiterPhonenum = waiterPhonenum;
    }

    @Override
    public String toString() {
        return "WaiterDO{" +
                "waiterId=" + waiterId +
                ", allUserDO=" + allUserDO +
                ", waiterName='" + waiterName + '\'' +
                ", waiterSex=" + waiterSex +
                ", waiterAge=" + waiterAge +
                ", waiterEntryTime='" + waiterEntryTime + '\'' +
                ", waiterPhonenum='" + waiterPhonenum + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WaiterDO)) return false;
        WaiterDO waiterDO = (WaiterDO) o;
        return Objects.equals(waiterId, waiterDO.waiterId) &&
                Objects.equals(allUserDO, waiterDO.allUserDO) &&
                Objects.equals(waiterName, waiterDO.waiterName) &&
                Objects.equals(waiterSex, waiterDO.waiterSex) &&
                Objects.equals(waiterAge, waiterDO.waiterAge) &&
                Objects.equals(waiterEntryTime, waiterDO.waiterEntryTime) &&
                Objects.equals(waiterPhonenum, waiterDO.waiterPhonenum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(waiterId, allUserDO, waiterName, waiterSex, waiterAge, waiterEntryTime, waiterPhonenum);
    }
}
