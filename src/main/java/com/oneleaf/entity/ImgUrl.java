package com.oneleaf.entity;

/**
 * 用来在控制层和视图层之间传输上传图片的地址的VO类
 *
 */
public class ImgUrl {
	/**
	 * 完整路径 http://localhost:8090/fileserver/upload/xxxx.jpg
	 */
	private String url;
	/**
	 * 部分路径 /xxxx.jpg
	 */
	private String partUrl;

	public String getPartUrl() {
		return partUrl;
	}

	public void setPartUrl(String partUrl) {
		this.partUrl = partUrl;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((partUrl == null) ? 0 : partUrl.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImgUrl other = (ImgUrl) obj;
		if (partUrl == null) {
			if (other.partUrl != null)
				return false;
		} else if (!partUrl.equals(other.partUrl))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ImgUrl [url=" + url + ", partUrl=" + partUrl + "]";
	}

	
}
