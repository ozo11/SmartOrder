package com.oneleaf.entity;

import java.util.Objects;

public class NoticeDO extends BaseDO{
    private Integer noticeId;
    private Integer adminId;
    private String noticeTime;
    private String noticeContent;

    public Integer getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(Integer noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getAdminId() {
        return adminId;
    }

    public void setAdminId(Integer adminId) {
        this.adminId = adminId;
    }

    public String getNoticeTime() {
        return noticeTime;
    }

    public void setNoticeTime(String noticeTime) {
        this.noticeTime = noticeTime;
    }

    public String getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent;
    }

    public NoticeDO(Integer noticeId, Integer adminId, String noticeTime, String noticeContent) {
        this.noticeId = noticeId;
        this.adminId = adminId;
        this.noticeTime = noticeTime;
        this.noticeContent = noticeContent;
    }

    public NoticeDO() {
    }

    @Override
    public String toString() {
        return "NoticeDO{" +
                "noticeId=" + noticeId +
                ", adminId=" + adminId +
                ", noticeTime='" + noticeTime + '\'' +
                ", noticeContent='" + noticeContent + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoticeDO noticeDO = (NoticeDO) o;
        return Objects.equals(noticeId, noticeDO.noticeId) &&
                Objects.equals(adminId, noticeDO.adminId) &&
                Objects.equals(noticeTime, noticeDO.noticeTime) &&
                Objects.equals(noticeContent, noticeDO.noticeContent);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noticeId, adminId, noticeTime, noticeContent);
    }
}

