package com.oneleaf.service;

import com.oneleaf.entity.WaiterDO;

import java.util.List;

public interface WaiterService {

    /**
     * 查询所有服务员人员信息
     * @return
     */
    public List<WaiterDO> listWaiter();

    /**
     * 按分页+条件查询所有用户信息
     * @return
     */public List<WaiterDO> listWaiterByCondition(WaiterDO waiterDO);

    /**
     * 服务员记录总数
     * @param waiterDO
     * @return
     */
    public Long getWaiterCount(WaiterDO waiterDO);

    /**
     * 根据条件查询服务员人员数据
     * @param waiterDO 查询条件(id,account,password)
     * @return
     */
    public WaiterDO getUpdateWaiter(WaiterDO waiterDO);

    /**
     * 更新服务员人员信息
     * @param waiterDO
     */
    public void updateWaiter(WaiterDO waiterDO);

    /**
     * 删除服务员
     * @param waiterDO
     * @return
     */
    public void deleteWaiter(WaiterDO waiterDO);

}
