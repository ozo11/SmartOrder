package com.oneleaf.service;

import com.oneleaf.entity.AllUserDO;

public interface AllUserService {
    /**
     * 用户登录
     * @param allUserDO 用户输入的登录信息(account,password)
     * @return 用户登录成功，则返回该用户的所有信息，如果登录失败，返回null
     */
    public AllUserDO login(AllUserDO allUserDO);

    /**
     * 添加用户
     * @param allUserDO
     * @return
     */
    public void saveAllUser(AllUserDO allUserDO);

    /**
     * 更新用户
     * @param allUserDO
     * @return
     */
    public void updateAllUser(AllUserDO allUserDO);

    /**
     * 删除用户
     * @param allUserDO
     * @return
     */
    public void deleteAllUser(AllUserDO allUserDO);
}
