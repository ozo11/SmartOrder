package com.oneleaf.service;

import com.oneleaf.entity.KitchenDO;

import java.util.List;

public interface KitchenService {

    /**
     * 查询所有后厨人员信息
     * @return
     */
    public List<KitchenDO> listKitchen();

    /**
     * 按分页+条件查询所有用户信息
     * @return
     */public List<KitchenDO> listKitchenByCondition(KitchenDO kitchenDO);

    /**
     * 后厨记录总数
     * @param kitchenDO
     * @return
     */
    public Long getKitchenCount(KitchenDO kitchenDO);

    /**
     * 根据条件查询后厨人员数据
     * @param kitchenDO 查询条件(id,account,password)
     * @return
     */
    public KitchenDO getUpdateKitchen(KitchenDO kitchenDO);

    /**
     * 更新后厨人员信息
     * @param kitchenDO
     */
    public void updateKitchen(KitchenDO kitchenDO);

    /**
     * 删除后厨
     * @param kitchenDO
     * @return
     */
    public void deleteKitchen(KitchenDO kitchenDO);

}
