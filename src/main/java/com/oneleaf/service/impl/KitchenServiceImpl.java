package com.oneleaf.service.impl;

import com.oneleaf.dao.KitchenDAO;
import com.oneleaf.entity.KitchenDO;
import com.oneleaf.service.KitchenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KitchenServiceImpl implements KitchenService {

    @Autowired
    private KitchenDAO kitchenDAO;

    /**
     * 查询所有后厨人员信息
     * @return
     */
    @Override
    public List<KitchenDO> listKitchen() {
        return kitchenDAO.listKitchen();
    }

    /**
     * 根据条件查询后厨人员信息
     * @param kitchenDO
     * @return
     */
    @Override
    public List<KitchenDO> listKitchenByCondition(KitchenDO kitchenDO) {
        /*修改模糊查询的参数
         * 10，20，11------>%10%,%20%
         */
        if(kitchenDO != null){
            if(kitchenDO.getKitchenName() != null && !kitchenDO.getKitchenName().equals("")){
                kitchenDO.setKitchenName("%" + kitchenDO.getKitchenName() + "%");
            }
        }
        return kitchenDAO.listKitchenByCondition(kitchenDO);
    }

    /**
     * 后厨记录总数
     * @param kitchenDO
     * @return
     */
    @Override
    public Long getKitchenCount(KitchenDO kitchenDO){
        /*修改模糊查询的参数
         * 10，20，11------>%10%,%20%
         */
        if(kitchenDO != null){
            if(kitchenDO.getKitchenName() != null && !kitchenDO.getKitchenName().equals("")){
                kitchenDO.setKitchenName("%" + kitchenDO.getKitchenName() + "%");
            }
        }
        return kitchenDAO.getKitchenCount(kitchenDO);
    }

    /**
     * 查询待更新后厨人员信息
     * @param kitchenDO
     */
    @Override
    public KitchenDO getUpdateKitchen(KitchenDO kitchenDO){
        return kitchenDAO.getKitchenByCondition(kitchenDO);
    }

    /**
     * 更新后厨人员信息
     * @param kitchenDO
     */
    @Override
    public void updateKitchen(KitchenDO kitchenDO){
        kitchenDAO.updateKitchen(kitchenDO);
    }

    /**
     * 删除后厨
     * @param kitchenDO
     * @return
     */
    public void deleteKitchen(KitchenDO kitchenDO){
        kitchenDAO.deleteKitchen(kitchenDO);
    }

}
