package com.oneleaf.service.impl;

import com.oneleaf.dao.OrderDao;
import com.oneleaf.entity.DishesDO;
import com.oneleaf.entity.DishesOrderDO;
import com.oneleaf.entity.OrderDO;
import com.oneleaf.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Override
    public List<DishesDO> findAllDishes(DishesDO dishesDO) {
        return orderDao.findAllDishes(dishesDO);
    }

    @Override
    public long getDishesCount(DishesDO dishesDO) {
        return orderDao.getDishesCount(dishesDO);
    }
    @Override
    public void saveOrderDishes(DishesOrderDO dishesOrderDO) {
        orderDao.saveOrderDishes(dishesOrderDO);
    }

    @Override
    public List<DishesOrderDO> findOrder(Integer tableNum) {
        return orderDao.findOrder(tableNum);
    }

    @Override
    public void saveOrder(OrderDO orderDO) {
        orderDao.saveOrder(orderDO);
    }

    @Override
    public List<OrderDO> findOffOrder() {
        return orderDao.findOffOrder();
    }

    @Override
    public void updateState(Integer id) {
        orderDao.updateState(id);
    }

    @Override
    public List<OrderDO> findOrderById(Integer id) {
        return orderDao.findOrderById(id);
    }
}
