package com.oneleaf.service.impl;

import com.oneleaf.dao.AllUserDAO;
import com.oneleaf.dao.DishesOrderDAO;
import com.oneleaf.entity.DishesOrderDO;
import com.oneleaf.service.DishesOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DishesOrderServiceimpl implements DishesOrderService {

    @Autowired
    private DishesOrderDAO dishesOrderDAO;

    @Override
    public List<DishesOrderDO> listDishesorder() {
        return dishesOrderDAO.listDishesorder();
    }

    @Override
    public void startDishesOrder(DishesOrderDO dishesOrderDO) {
        dishesOrderDAO.startDishesOrder(dishesOrderDO);
    }

    @Override
    public void endDishesOrder(DishesOrderDO dishesOrderDO) {
        dishesOrderDAO.endDishesOrder(dishesOrderDO);
    }

    @Override
    public List<DishesOrderDO> listCookedOrder() {
        return dishesOrderDAO.listCookedOrder();
    }

}
