package com.oneleaf.service.impl;

import com.oneleaf.dao.FoodDao;
import com.oneleaf.entity.DishesDO;
import com.oneleaf.entity.DishesImageDO;
import com.oneleaf.entity.DishesTypeDO;
import com.oneleaf.service.FoodService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FoodServiceImpl implements FoodService {
    @Autowired
    private FoodDao foodDao;
    @Override
    public List<DishesTypeDO> findAllDishesType() {
        return foodDao.findAllDishesType();
    }

    @Override
    public int saveFood(DishesDO dishesDO) {
        return foodDao.saveFood(dishesDO);
    }

    @Override
    public List<DishesDO> findAllDishes() {
        return foodDao.findAllDishes();
    }

    @Override
    public DishesDO findFoodById(Integer disheId) {
        return foodDao.findFoodById(disheId);
    }

    @Override
    public void updateFood(DishesDO dishesDO) {
        foodDao.updateFood(dishesDO);
    }

    @Override
    public void saveFile(DishesImageDO dishesImageDO){
        foodDao.saveFile(dishesImageDO);
    }

    @Override
    public List<DishesImageDO> findAllPicture(Integer id) {
        return foodDao.findAllPicture(id);
    }

    @Override
    public List<DishesDO> findDishesDO(String sourch) {
        return foodDao.findDishesDO(sourch);
    }

    @Override
    public List<DishesImageDO> testPicture() {
        return foodDao.testPicture();
    }

    @Override
    public void deleteFoodById(Integer id) {
        foodDao.deleteFoodById(id);
    }

    public static void main(String[] args) {
        FoodServiceImpl foodService = new FoodServiceImpl();
        System.out.println(foodService.findAllDishesType());
    }
}
