package com.oneleaf.service.impl;

import com.oneleaf.dao.AllUserDAO;
import com.oneleaf.entity.AllUserDO;
import com.oneleaf.entity.ImgUrl;
import com.oneleaf.service.AllUserService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

@Service
public class AllUserServiceImpl implements AllUserService {

    @Autowired
    private AllUserDAO allUserDAO;

    /**
     * 用户登录
     * @param allUserDO 用户输入的登录信息(account,password)
     * @return 用户登录成功，则返回该用户的所有信息，如果登录失败，返回null
     */
    @Override
    public AllUserDO login(AllUserDO allUserDO) {
        return allUserDAO.getAllUserDOByCondition(allUserDO);
    }

    /**
     * 添加用户
     * @param allUserDO
     * @return
     */
    @Override
    public void saveAllUser(AllUserDO allUserDO) {
        allUserDAO.saveAllUser(allUserDO);
    }

    /**
     * 更新用户
     * @param allUserDO
     * @return
     */
    public void updateAllUser(AllUserDO allUserDO){
        allUserDAO.updateAllUser(allUserDO);
    }

    /**
     * 删除用户
     * @param allUserDO
     * @return
     */
    public void deleteAllUser(AllUserDO allUserDO){
        allUserDAO.deleteAllUser(allUserDO);
    }

}
