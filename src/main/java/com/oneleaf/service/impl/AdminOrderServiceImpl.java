package com.oneleaf.service.impl;

import com.oneleaf.dao.AdminOrderDao;
import com.oneleaf.entity.OrderDO;
import com.oneleaf.service.AdminOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AdminOrderServiceImpl implements AdminOrderService {

    @Autowired
    private AdminOrderDao adminOrderDao;

    @Override
    public List<OrderDO> findNotEndOrder() {
        return adminOrderDao.findNotEndOrder();
    }

    @Override
    public int updateOrderState(OrderDO orderDO) {
        return adminOrderDao.updateOrderState(orderDO);
    }

    @Override
    public List<OrderDO> findEndOrder() {
        return adminOrderDao.findEndOrder();
    }


}
