package com.oneleaf.service.impl;

import com.oneleaf.dao.NoticeDAO;
import com.oneleaf.entity.NoticeDO;
import com.oneleaf.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    private NoticeDAO noticeDAO;

    @Override
    public List<NoticeDO> getNotice() {
        return noticeDAO.getNoticeDO();
    }

    @Override
    public List<NoticeDO> getNoticePage(NoticeDO noticeDO) {
        return noticeDAO.getNoticePage(noticeDO);
    }

    @Override
    public long getNoticeCount(NoticeDO noticeDO) {
        return noticeDAO.getNoticeCount(noticeDO);
    }

    @Override
    public void addNotice(NoticeDO noticeDO) {
        noticeDAO.addNoticeDO(noticeDO);
    }

    @Override
    public NoticeDO getNoticeById(Integer noticeid) {
        return noticeDAO.getNoticeByIdDAO(noticeid);
    }

    @Override
    public void deleteNoticeById(Integer noticeid) {
        noticeDAO.deleteNoticeById(noticeid);
    }

    @Override
    public void updateNotice2(NoticeDO noticeDO) {
        noticeDAO.updateNotice2(noticeDO);
    }
}
