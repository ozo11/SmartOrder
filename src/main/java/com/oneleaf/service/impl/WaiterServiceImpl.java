package com.oneleaf.service.impl;

import com.oneleaf.dao.WaiterDAO;
import com.oneleaf.entity.WaiterDO;
import com.oneleaf.service.WaiterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WaiterServiceImpl implements WaiterService {

    @Autowired
    private WaiterDAO waiterDAO;

    /**
     * 查询所有服务员人员信息
     * @return
     */
    @Override
    public List<WaiterDO> listWaiter() {
        return waiterDAO.listWaiter();
    }

    /**
     * 根据条件查询服务员人员信息
     * @param waiterDO
     * @return
     */
    @Override
    public List<WaiterDO> listWaiterByCondition(WaiterDO waiterDO) {
        /*修改模糊查询的参数
         * 10，20，11------>%10%,%20%
         */
        if(waiterDO != null){
            if(waiterDO.getWaiterName() != null && !waiterDO.getWaiterName().equals("")){
                waiterDO.setWaiterName("%" + waiterDO.getWaiterName() + "%");
            }
        }
        return waiterDAO.listWaiterByCondition(waiterDO);
    }

    /**
     * 服务员记录总数
     * @param waiterDO
     * @return
     */
    @Override
    public Long getWaiterCount(WaiterDO waiterDO){
        /*修改模糊查询的参数
         * 10，20，11------>%10%,%20%
         */
        if(waiterDO != null){
            if(waiterDO.getWaiterName() != null && !waiterDO.getWaiterName().equals("")){
                waiterDO.setWaiterName("%" + waiterDO.getWaiterName() + "%");
            }
        }
        return waiterDAO.getWaiterCount(waiterDO);
    }

    /**
     * 查询待更新服务员人员信息
     * @param waiterDO
     */
    @Override
    public WaiterDO getUpdateWaiter(WaiterDO waiterDO){
        return waiterDAO.getWaiterByCondition(waiterDO);
    }

    /**
     * 更新服务员人员信息
     * @param waiterDO
     */
    @Override
    public void updateWaiter(WaiterDO waiterDO){
        waiterDAO.updateWaiter(waiterDO);
    }

    /**
     * 删除服务员
     * @param waiterDO
     * @return
     */
    public void deleteWaiter(WaiterDO waiterDO){
        waiterDAO.deleteWaiter(waiterDO);
    }

}
