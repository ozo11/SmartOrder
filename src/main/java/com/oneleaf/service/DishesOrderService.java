package com.oneleaf.service;

import com.oneleaf.entity.DishesOrderDO;

import java.util.List;

public interface DishesOrderService {

    /**
     * 显示点菜菜单信息
     * @return
     */
    public List<DishesOrderDO> listDishesorder();

    /**
     * 开始烹饪
     */
    public void startDishesOrder(DishesOrderDO dishesOrderDO);

    public void endDishesOrder(DishesOrderDO dishesOrderDO);

    public List<DishesOrderDO> listCookedOrder();
}
