package com.oneleaf.service;

import com.oneleaf.entity.NoticeDO;

import java.util.List;

public interface NoticeService {
    /*查询公告*/
    public List<NoticeDO> getNotice();
    /*查询公告分页*/
    public List<NoticeDO> getNoticePage(NoticeDO noticeDO);
    //查找公告条数
    public long getNoticeCount(NoticeDO noticeDO);
    /*添加公告*/
    public void addNotice(NoticeDO noticeDO);
    /*通过id查公告*/
    public NoticeDO getNoticeById(Integer noticeid);

    public void deleteNoticeById(Integer noticeid);

    public void updateNotice2(NoticeDO noticeDO);
}
