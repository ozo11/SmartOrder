package com.oneleaf.dao;

import com.oneleaf.entity.NoticeDO;

import java.util.List;

public interface NoticeDAO {
    /**
     * 查找所有公告
     */
    public List<NoticeDO> getNoticeDO();

    /**
     * 查找所有公告分页
     */
    public List<NoticeDO> getNoticePage(NoticeDO noticeDO);
    /**
     * 查找公告条数
     */
    public long getNoticeCount(NoticeDO noticeDO);

    /**
     * 添加公告
     */
    public void addNoticeDO(NoticeDO noticeDO);
    /*
    * 通过id查公告
    * */
    public NoticeDO getNoticeByIdDAO(Integer noticeid);
    /*
     * 通过id删除公告
     * */
    public void deleteNoticeById(Integer noticeid);
    /*
     * 更新公告
     * */
    public void updateNotice2(NoticeDO noticeDO);
}
