package com.oneleaf.dao;

import com.oneleaf.entity.DishesDO;
import com.oneleaf.entity.DishesImageDO;
import com.oneleaf.entity.DishesTypeDO;

import java.util.List;

public interface FoodDao {
    //查找所有菜品种类
    List<DishesTypeDO> findAllDishesType();
    //插入菜品信息
    int saveFood(DishesDO dishesDO);
    //查看所有菜品信息
    List<DishesDO> findAllDishes();
    //查找菜品
    DishesDO findFoodById(Integer id);
    //修改菜品信息
    void updateFood(DishesDO dishesDO);
    //进入文件上传界面
    void saveFile(DishesImageDO dishesImageDO);
    //测试方法
    List<DishesImageDO> testPicture();
    //删除菜品信息
    void deleteFoodById(Integer id);
    //查找商品所有图片
    List<DishesImageDO> findAllPicture(Integer id);
    //查找商品
    List<DishesDO> findDishesDO(String sourch);
}
