package com.oneleaf.dao;

import com.oneleaf.entity.WaiterDO;

import java.util.List;

public interface WaiterDAO {

    /**
     * 查询所有服务员人员信息
     * @return
     */
    public List<WaiterDO> listWaiter();

    /**
     * 按条件和分页信息查询服务员人员记录
     * @param waiterDO 包含了条件和分页信息（userAccount,departmentInfoDO.departmentId,start,length）
     * @return
     */
    public List<WaiterDO> listWaiterByCondition(WaiterDO waiterDO);

    /**
     * 按条件查询服务员记录总数
     * @param waiterDO
     * @return
     */
    public Long getWaiterCount(WaiterDO waiterDO);

    /**
     * 根据条件查询服务员人员数据
     * @param waiterDO 查询条件(id,account,password)
     * @return
     */
    public WaiterDO getWaiterByCondition(WaiterDO waiterDO);

    /**
     * 更新服务员人员信息
     * @param waiterDO
     */
    public void updateWaiter(WaiterDO waiterDO);

    /**
     * 删除服务员
     * @param waiterDO
     * @return
     */
    public void deleteWaiter(WaiterDO waiterDO);

}
