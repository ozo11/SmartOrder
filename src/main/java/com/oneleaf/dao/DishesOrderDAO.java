package com.oneleaf.dao;

import com.oneleaf.entity.DishesOrderDO;

import java.util.List;

public interface DishesOrderDAO {

    public List<DishesOrderDO> listDishesorder();

    public void startDishesOrder(DishesOrderDO dishesOrderDO);

    public void endDishesOrder(DishesOrderDO dishesOrderDO);

    public List<DishesOrderDO> listCookedOrder();
}
