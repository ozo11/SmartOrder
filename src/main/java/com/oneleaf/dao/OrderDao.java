package com.oneleaf.dao;

import com.oneleaf.entity.DishesDO;
import com.oneleaf.entity.DishesOrderDO;
import com.oneleaf.entity.OrderDO;
import org.springframework.core.annotation.Order;

import java.util.List;

public interface OrderDao {

    //查看所有菜品信息
    List<DishesDO> findAllDishes(DishesDO dishesDO);
    //查找菜品条数
    long getDishesCount(DishesDO dishesDO);
    //创建点菜菜单信息
    void saveOrderDishes(DishesOrderDO dishesOrderDO);
    //查找桌号
    List<DishesOrderDO> findOrder(Integer tableNum);
    //插入订单信息表
    void saveOrder(OrderDO orderDO);
    //查找订单结算信息
    List<OrderDO> findOffOrder();
    //更新订单结算状态
    void updateState(Integer id);
    //查找订单信息
    List<OrderDO> findOrderById(Integer id);
}
