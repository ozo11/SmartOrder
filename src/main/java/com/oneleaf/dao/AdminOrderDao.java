package com.oneleaf.dao;

import com.oneleaf.entity.OrderDO;

import java.util.List;

public interface AdminOrderDao {
    //查找未结算订单
    List<OrderDO> findNotEndOrder();
    //修改订单状态
    int updateOrderState(OrderDO orderDO);
    //查找结算完成订单
    List<OrderDO> findEndOrder();
}
