package com.oneleaf.dao;

import com.oneleaf.entity.AllUserDO;

public interface AllUserDAO {

    /**
     * 根据条件查询用户数据
     * @param allUserDO 查询条件(id,account,password)
     * @return
     */
    public AllUserDO getAllUserDOByCondition(AllUserDO allUserDO);

    /**
     * 添加用户
     * @param allUserDO
     * @return
     */
    public void saveAllUser(AllUserDO allUserDO);

    /**
     * 更新用户
     * @param allUserDO
     * @return
     */
    public void updateAllUser(AllUserDO allUserDO);

    /**
     * 删除用户
     * @param allUserDO
     * @return
     */
    public void deleteAllUser(AllUserDO allUserDO);
}
