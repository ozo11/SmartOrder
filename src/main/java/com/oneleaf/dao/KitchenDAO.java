package com.oneleaf.dao;

import com.oneleaf.entity.KitchenDO;

import java.util.List;

public interface KitchenDAO {

    /**
     * 查询所有后厨人员信息
     * @return
     */
    public List<KitchenDO> listKitchen();

    /**
     * 按条件和分页信息查询后厨人员记录
     * @param kitchenDO 包含了条件和分页信息（userAccount,departmentInfoDO.departmentId,start,length）
     * @return
     */
    public List<KitchenDO> listKitchenByCondition(KitchenDO kitchenDO);

    /**
     * 按条件查询后厨记录总数
     * @param kitchenDO
     * @return
     */
    public Long getKitchenCount(KitchenDO kitchenDO);

    /**
     * 根据条件查询后厨人员数据
     * @param kitchenDO 查询条件(id,account,password)
     * @return
     */
    public KitchenDO getKitchenByCondition(KitchenDO kitchenDO);

    /**
     * 更新后厨人员信息
     * @param kitchenDO
     */
    public void updateKitchen(KitchenDO kitchenDO);

    /**
     * 删除后厨
     * @param kitchenDO
     * @return
     */
    public void deleteKitchen(KitchenDO kitchenDO);

}
